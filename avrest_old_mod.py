#Утилиты
import wikipedia
import wolframalpha
#Ключи
from key_generator.key_generator import generate
import mariadb
import secrets
#NC
import discord
from discord.ext import commands, tasks
#other
import asyncio
import random
from random import randint
#Google
import gspread
from oauth2client.service_account import ServiceAccountCredentials
#json
import json

with open('avrest/config.json', 'r') as avrest_const:
    avrest_const = json.load(avrest_const)

intents = discord.Intents.default()

avrest_current_version = avrest_const["version"]

gc = gspread.service_account(filename='avrest/mycredentials.json')
sh = gc.open_by_url(avrest_const["g_tables"])
db_codes = sh.worksheet("codes")

def get_guild_prefix(bot,message):
    try:
        cursor.execute("SELECT * FROM guilds_data WHERE guild_id=%i" % (message.guild.id))
        g_id = cursor.fetchone()
        return g_id[6]
    except:
        return avrest_const["prefix"]           

bot = commands.Bot(command_prefix = get_guild_prefix, intents=intents)

client = wolframalpha.Client(avrest_const['wolphtok'])

cnx = mariadb.connect(user=avrest_const["db"]["login"], password=avrest_const["db"]["passwd"],host=avrest_const["db"]["host"],port=int(avrest_const["db"]["port"]),database=avrest_const["db"]["database"])

cnx.autocommit = True
cursor = cnx.cursor()

bot.remove_command('help')

@bot.command(name="help", aliases=['hlp', 'h'], description='Справка.')
async def help(ctx, choice = None ):
    try:
        cursor.execute("SELECT * FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
        g_id = cursor.fetchone()
        preff = g_id[6]
    except:
        preff = "-"
    try:
        choice = choice.lower()
    except:
        choice = choice
    if (choice==None):
        embed = discord.Embed(color = 0x000001, title = "... Ну не работает... А кому сейчас легко?)") # Создание Embed'a
        embed.add_field(name="avrest", value="Основные команды.", inline=False)
        embed.add_field(name="graph", value="Команды ассистента", inline=False)
        embed.add_field(name="dreamer", value="Музыкальные команды.", inline=False)
        embed.set_footer(text=f"Подробнее о каждой команде [{preff}help _command_].")
    elif (choice == 'avrest') or (choice == 'main'):
        embed = discord.Embed(color = 0x258141, title = 'Avrest - Main core') # Создание Embed'a
        embed.add_field(name="choose  _[items]_", value="Выбор случайного элемента.", inline=False)
        embed.add_field(name="balance", value="Узнать баланс.", inline=False)
        embed.add_field(name="use.ticket _ticket_", value="Использовать код (купон).", inline=False)
        embed.add_field(name="give _user_ _amount_", value="Перевести пользователю указанное количество очков (облагается налогом).", inline=False)
        embed.add_field(name="rank.up", value="Повысить ранг.", inline=False)
        embed.add_field(name="rank.info", value="Информация о повышении ранга.", inline=False)
        embed.add_field(name="solve", value="Решить произвольную задачу (любой тип).", inline=False)
        embed.add_field(name="search", value="Узнать информацию (эталон).", inline=False)
        embed.add_field(name="search.eng", value="Узнать информацию англоязычный поиск (эталон).", inline=False)
        embed.add_field(name="lott _amount_", value="Поставить некоторое количество очков (все зависит от вашей удачи).", inline=False)
        embed.add_field(name="stocks.buy _amount_", value="Купить токен активности сервера.", inline=False)
        embed.add_field(name="stocks.sell _amount_", value="Продать токен активности сервера.", inline=False)
        embed.add_field(name="stocks", value="Информация о токенах активности сервера.", inline=False)
        embed.add_field(name="stream", value="Получить права стримера (временно).", inline=False)
        embed.add_field(name="recomend _user_", value="Рекомендовать пользователя на повышение ранга.", inline=False)
        embed.add_field(name="join.to _user_", value="Присоедениться к голосовому каналу по пользователю.", inline=False)
        
        embed.add_field(name="avrest.start", value="Настройка бота.", inline=False)
        embed.add_field(name="reg", value="Регистрация/Восстановление в сервисах [AG].", inline=False)
        embed.add_field(name="report", value="Сообщить о нарушении правил/о баге.", inline=False)
        embed.add_field(name="bug", value="Сообщить о баге. (Если не хотите использовать команду, можете отправить письмо на fool@avrest.ru с полным описанием проблемы.)", inline=False)
        embed.set_footer(text=f"Подробнее о каждой команде [{preff}help _command_].")
    elif (choice == 'choose'):
        embed = discord.Embed(color = 0x258141, title = 'Choose') # Создание Embed'a
        embed.add_field(name="choose  _[items]_", value=f"Выбор случайного элемента, из списка элементов введенных в команду через пробел. прим [{preff}choose one two three]", inline=False)
        embed.set_footer(text=f"Если команда выполняется не корректно и/или не выполняется используйте [{preff}bug _[content]_], опишите проблему.")
    elif (choice == 'balance'):
        embed = discord.Embed(color = 0x258141, title = 'Choose') # Создание Embed'a
        embed.add_field(name="balance", value=f"Узнать свой баланс в группе гильдий к которой пренадлежит сервер. прим [{preff}balance], возможна модификация [{preff}balance p] которая публикует ответ в текстовый канал", inline=False)
        embed.set_footer(text=f"Если команда выполняется не корректно и/или не выполняется используйте [{preff}bug _[content]_], опишите проблему.")
    elif (choice == 'use.ticket'):
        embed = discord.Embed(color = 0x258141, title = 'Use.Ticket') # Создание Embed'a
        embed.add_field(name="use.ticket _ticket_", value=f"Использовать код (купон), примененная команда удаляется. прим [{preff}use.ticket one-two-three]", inline=False)
        embed.set_footer(text=f"Если команда выполняется не корректно и/или не выполняется используйте [{preff}bug _[content]_], опишите проблему.")
    elif (choice == 'give'):
        embed = discord.Embed(color = 0x258141, title = 'Give') # Создание Embed'a
        embed.add_field(name="give _uset_ _amount_", value=f"Перевести пользователю указанное количество очков, облагается налогом, налог определяется гильдией. прим [{preff}give @Woozy 100]", inline=False)
        embed.set_footer(text=f"Если команда выполняется не корректно и/или не выполняется используйте [{preff}bug _[content]_], опишите проблему.")
    elif (choice == 'rank.up'):
        embed = discord.Embed(color = 0x258141, title = 'Rank.Up') # Создание Embed'a
        embed.add_field(name="rank.up", value=f"Повысить ранг, при не соответствии условиям повышения, выводит не соответствующие параметры, можно использовать для восстановления ранга. прим [{preff}rank.up]", inline=False)
        embed.set_footer(text=f"Если команда выполняется не корректно и/или не выполняется используйте [{preff}bug _[content]_], опишите проблему.")
    elif (choice == 'rank.info'):
        embed = discord.Embed(color = 0x258141, title = 'Rank.Info') # Создание Embed'a
        embed.add_field(name="rank.info", value=f"Информация о повышении ранга. прим [{preff}rank.info]", inline=False)
        embed.set_footer(text=f"Если команда выполняется не корректно и/или не выполняется используйте [{preff}bug _[content]_], опишите проблему.")
    elif (choice == 'solve'):
        embed = discord.Embed(color = 0x258141, title = 'Solve') # Создание Embed'a
        embed.add_field(name="solve", value=f"Решить произвольную задачу (любой тип). прим [{preff}solve Russia]", inline=False)
        embed.set_footer(text=f"Если команда выполняется не корректно и/или не выполняется используйте [{preff}bug _[content]_], опишите проблему.")
    elif (choice == 'search'):
        embed = discord.Embed(color = 0x258141, title = 'Search') # Создание Embed'a
        embed.add_field(name="search", value=f"Узнать информацию, запрос можно выполнить на любом языке, возвращает данные на Русском языке (эталон - при установлении решений модерация опирается в первую очередь на полученные данные). прим [{preff}search Russia]", inline=False)
        embed.set_footer(text=f"Если команда выполняется не корректно и/или не выполняется используйте [{preff}bug _[content]_], опишите проблему.")
    elif (choice == 'search.eng'):
        embed = discord.Embed(color = 0x258141, title = 'Search.Eng') # Создание Embed'a
        embed.add_field(name="search.eng", value=f"Узнать информацию, запрос можно выполнить на любом языке, возвращает данные на Английском языке (эталон - при установлении решений модерация опирается в первую очередь на полученные данные). прим [{preff}search.eng Russia]", inline=False)
        embed.set_footer(text=f"Если команда выполняется не корректно и/или не выполняется используйте [{preff}bug _[content]_], опишите проблему.")
    elif (choice == 'lott'):
        embed = discord.Embed(color = 0x258141, title = 'Lott') # Создание Embed'a
        embed.add_field(name="lott _amount_", value=f"Поставить некоторое количество очков (все зависит от вашей удачи) в случае выйгрыша вычитается налог сервера {g_id[13]}%, возможно использование модификаторов all/half/quarter/quaver. прим [{preff}lott 100] или [{preff}lott all]", inline=False)
        embed.set_footer(text=f"Если команда выполняется не корректно и/или не выполняется используйте [{preff}bug _[content]_], опишите проблему.")
    elif (choice == 'stocks.buy'):
        embed = discord.Embed(color = 0x258141, title = 'Stocks.Buy') # Создание Embed'a
        embed.add_field(name="stocks.buy _amount_", value=f"Купить токен активности сервера, токены активности начисляют {g_id[17]} очков на каждую еденицу токена ежедневно гарантированно, +- некоторое фиатное значение. Не более {g_id[19]} токенов на пользователя. прим [{preff}stocks.buy 10]", inline=False)
        embed.set_footer(text=f"Если команда выполняется не корректно и/или не выполняется используйте [{preff}bug _[content]_], опишите проблему.")
    elif (choice == 'stocks.sell'):
        embed = discord.Embed(color = 0x258141, title = 'Stocks.Sell') # Создание Embed'a
        embed.add_field(name="stocks.sell _amount_", value=f"Продать токен активности сервера. прим [{preff}stocks.sell 10]", inline=False)
        embed.set_footer(text=f"Если команда выполняется не корректно и/или не выполняется используйте [{preff}bug _[content]_], опишите проблему.")
    elif (choice == 'stocks'):
        embed = discord.Embed(color = 0x258141, title = 'Stocks') # Создание Embed'a
        embed.add_field(name="stocks", value=f"Информация о токенах активности сервера. прим [{preff}stocks]", inline=False)
        embed.set_footer(text=f"Если команда выполняется не корректно и/или не выполняется используйте [{preff}bug _[content]_], опишите проблему.")   
    elif (choice == 'recomend'):
        embed = discord.Embed(color = 0x258141, title = 'Recomend') # Создание Embed'a
        embed.add_field(name="recomend _user_", value=f"Рекомендовать пользователя на повышение ранга, за повышение списывается количество очков требуемое для достижения максимального ранга * 0.5. прим [{preff}recomend @Woozy]", inline=False)
        embed.set_footer(text=f"Если команда выполняется не корректно и/или не выполняется используйте [{preff}bug _[content]_], опишите проблему.")
    elif (choice == 'join.to'):
        embed = discord.Embed(color = 0x258141, title = 'Join.To') # Создание Embed'a
        embed.add_field(name="join.to _user_", value=f"Присоедениться к голосовому каналу по пользователю, для использования обоим пользователям необходимо быть в голосовых каналах, прользователь выполняющий команду молжен иметь доступ к каналу на который требуется примоедениться. прим [{preff}join.to @Woozy]", inline=False)
        embed.set_footer(text=f"Если команда выполняется не корректно и/или не выполняется используйте [{preff}bug _[content]_], опишите проблему.")
    elif (choice == 'stream'):
        embed = discord.Embed(color = 0x258141, title = 'Stream') # Создание Embed'a
        embed.add_field(name="stream", value="Получить права стримера на 5 минут, работающие стримы не прервутся. прим [{preff}stream]", inline=False)
        embed.set_footer(text=f"Если команда выполняется не корректно и/или не выполняется используйте [{preff}bug _[content]_], опишите проблему.")   
    elif (choice == 'avrest.start'):
        embed = discord.Embed(color = 0x258141, title = 'Avrest.Start') # Создание Embed'a
        embed.add_field(name="avrest.start _1_ _2_ _3_ _4_ _5_ _6_ _7_ _8_", value=f"Настройка бота.", inline=False)
        embed.add_field(name="ISO", value="Создание изолированного сервераю", inline=False)
        embed.add_field(name="_1_", value="Тип гильдии iso", inline=False)
        embed.add_field(name="_2_", value="Название валюты и стартовое значение ppt:0", inline=False)
        embed.add_field(name="_3_", value="Ранг:стоимость повышения. HI-rank:0,H-rank:250,G-rank:1250,F-rank:6250,E-rank:32000,D-rank:160000,C-rank:800000,B-rank:4000000,A-rank:20000000,S-rank:10000000", inline=False)
        embed.add_field(name="_4_", value="Количество очков котое пользователь получает за сообщение 1", inline=False)
        embed.add_field(name="_5_", value="Название токена гильдии:стоимость покупки:стоимость продажи:ключевая ставка:всего токенов stocks:10000:5000:50:10000", inline=False)
        embed.add_field(name="_6_", value="Налог лотереи 5", inline=False)
        embed.add_field(name="_7_", value="Налог перевода:переводы без налога 1_100,5_1000,15_10000,25_25000,30_50000,50_100000:1000 ", inline=False)
        embed.add_field(name="_8_", value="Автоманическая генерация каналов и рангов 1", inline=False)
        embed.add_field(name="Доп:", value=f"Для более точной настройки или изменения настроек бота [{preff}avrest.core]", inline=False)
        embed.add_field(name="Group", value="Подключение к другому изолированному серверу.", inline=False)
        embed.add_field(name="_1_", value="ID подключения", inline=False)
        embed.add_field(name="_2_", value="публичный ключ гильдии", inline=False)
        embed.add_field(name="_3_", value="Автоманическая генерация каналов и рангов 1", inline=False)
        embed.add_field(name="AG", value="Создание изолированного сервераю", inline=False)
        embed.add_field(name="_1_", value="Тип гильдии ag", inline=False)
        embed.add_field(name="_2_", value="1", inline=False)
        embed.add_field(name="_3_", value="Автоманическая генерация каналов и рангов 1", inline=False)
        embed.set_footer(text=f"Если команда выполняется не корректно и/или не выполняется используйте [{preff}bug _[content]_], опишите проблему.")  
    elif (choice == 'reg'):
        embed = discord.Embed(color = 0x258141, title = 'Reg') # Создание Embed'a
        embed.add_field(name="reg", value=f"Регистрация/Восстановление в сервисах [AG]. прим [{preff}reg]", inline=False)
        embed.set_footer(text=f"Если команда выполняется не корректно и/или не выполняется используйте [{preff}bug _[content]_], опишите проблему.")  
    elif (choice == 'report'):
        embed = discord.Embed(color = 0x258141, title = 'Report') # Создание Embed'a
        embed.add_field(name="report _content_", value=f"Сообщить о нарушении правил/о баге. прим [{preff}report Какой ужас, щас такое расскажу, ты не представляешь...]", inline=False)
        embed.set_footer(text=f"Если команда выполняется не корректно и/или не выполняется используйте [{preff}bug _[content]_], опишите проблему.")  
            
    elif (choice == 'graph') or (choice == 'assistant'):
        ...
    elif (choice == 'dreamer') or (choice == 'music'):
        embed = discord.Embed(color = 0x258141, title = 'Dreamer - Music core') # Создание Embed'a
        embed.add_field(name="join|connect|j", value="Позвать бота в голосовой канал.", inline=False)
        embed.add_field(name="play|sing|p", value="Воспроизвести|добавить в очередь.", inline=False)
        embed.add_field(name="pause", value="Приостановить.", inline=False)
        embed.add_field(name="resume", value="Продолжить.", inline=False)
        embed.add_field(name="skip", value="Пропустить трек.", inline=False)
        embed.add_field(name="remove|rm|rem", value="Убрать трек из очереди.", inline=False)
        embed.add_field(name="clearqueue|clrq|clq|cq", value="Очистить очередь.", inline=False)
        embed.add_field(name="queue|q|playlist|que", value="Показать очередь.", inline=False)
        embed.add_field(name="np|song|current|currentsong|playing", value="Текущий трек.", inline=False)
        embed.add_field(name="volume|vol|v", value="Изменить громкость default:20%.", inline=False)
        embed.add_field(name="leave|stop|dc|disconnect|bye", value="Отключить воспроизведение треков.", inline=False)
    elif (choice == 'avrest.core'):
        embed = discord.Embed(color = 0x258141, title = 'Avrest.core bot.set') # Создание Embed'a
        embed.add_field(name="-avrest.core _[com]_  _[temp]_", value="Где _[com]_ подкоманда, а _[temp]_ значение. [MYSQL data.type]", inline=False)
        embed.add_field(name="bot.set.prefix _[prefix]_", value="Префикс может быть любым набором от 1 до 6 симовлов, по умолчанию [-]. [VARCHAR(8)]", inline=False)
        embed.add_field(name="bot.set.channel.prefix _[prefix]_", value="Префикс автогенерируемых голосовых каналов [prefix_user.name_postfix] может состоять из любово набора символов, общей длинной не превышающей 8 символов, по умолчанию [None]. [VARCHAR(16)]", inline=False)
        embed.add_field(name="bot.set.channel.postfix _[postfix]_", value="Постфикс автогенерируемых голосовых каналов [prefix_user.name_postfix] может состоять из любово набора символов, общей длинной не превышающей 8 символов, по умолчанию [_room]. [VARCHAR(16)]", inline=False)
        embed.add_field(name="bot.set.main.channel.category.name _[name]_", value="Изменить название категории создающего канала, не длиннее 16 символов, по умолчанию [~[AG]~]. [VARCHAR(16)]", inline=False)
        embed.add_field(name="bot.set.channel.category.name _[name]_", value="Изменить название категории в которую создаются комнаты, не длиннее 16 символов, по умолчанию [rooms]. [VARCHAR(16)]", inline=False)
        embed.add_field(name="bot.set.main.channel.name _[name]_", value="Изменить название создающего канала, не длиннее 16 символов, по умолчанию [Create_Room] [VARCHAR(16)].", inline=False)
        embed.add_field(name="bot.set.message.value _[value]_", value="Количество валюты которое пользователь получает за 1 сообщение, не должно превышать 127 едениц, по умолчанию [1]. [TINYINT]", inline=False)
        embed.add_field(name="bot.set.currency.name _[name]_", value="Название валюты, длинна не должна превышать 16 символов, по умолчанию [Ppt]. [VARCHAR(32)]", inline=False)
        embed.add_field(name="bot.set.guild.rangs _[rangs]_", value="Ранги гильдии, [_rang_:_value_,], по умолчанию ['H-rank:250,G-rank:1250,F-rank:6250,E-rank:32000,D-rank:160000,C-rank:800000,B-rank:4000000,A-rank:20000000,S-rank:100000000']. [VARCHAR(512)]", inline=False)
        embed.add_field(name="bot.set.guild.taxs _[tax]_", value="Налоги гильдии на внутренние переводы [-give], [-1] для отключения налогов, [%_value,...:taxfree], по умолчанию ['1_100,5_1000,15_10000,25_25000,30_50000,50_100000:1000']. [VARCHAR(512)]", inline=False)
        embed.add_field(name="bot.set.lott.activity _[temp]_", value="Доступность команды [-lott], 0/1, по умолчанию [1]. [BOOLEAN]", inline=False)
        embed.add_field(name="bot.set.lott.tax _[temp]_", value="Налог [-lott], по умолчанию [5]%. [TINYINT]", inline=False)
        embed.add_field(name="bot.set.stocks.name _[name]_", value="Название фиатного токена, не более 16 символов, по умолчанию [stocks]. [VARCHAR(32)]", inline=False)
        embed.add_field(name="bot.set.stocks.buy _[temp]_", value="Количество валюты для покупки 1 еденицы токена, не более 1.000.000.000, по умолчанию [10000]. [INT]", inline=False)
        embed.add_field(name="bot.set.stocks.sell _[temp]_", value="Количество валюты при продажи 1 еденицы токена, не более 1.000.000.000, по умолчанию [5000]. [INT]", inline=False)
        embed.add_field(name="bot.set.stocks.key _[temp]_", value="Количество валюты получаемой за тик[1day] гарантированно, на 1 еденицу токена, не более 1.000.000.000, по умолчанию [50]. [INT]", inline=False)
        embed.add_field(name="bot.set.stocks.number _[temp]_", value="Количество едениц токена, не более 100.000.000, на момент изменения уже существующие токены не учитываются, по умолчанию [10000]. [INT]", inline=False)
        embed.add_field(name="bot.set.rang.promote _[temp]_", value="Работа алгоритма продвижения, при включении для продвижения к высоким рангам требуются рекомендации, 0/1, по умолчанию [1]. [BOOLEAN]", inline=False)
        embed.add_field(name="bot.set.status.tear _[temp]_", value="Система статусов пользователей, [status:transcript,...], не более 768 символов, по умолчанию []. [VARCHAR(1024)]", inline=False)
        embed.set_footer(text="Применение core команд вне рекомендаций ведет к НЕКОНТРОЛИРУЕМЫМ последствиям. Core команды типа SET доступны только администраторам.")
    else:
        embed = discord.Embed(color = 0x000001, title = 'Error') # Создание Embed'a
    await ctx.author.send(embed = embed)


#status_tier = # статус_расшифровка:статус_расшифровка...
@bot.command(name="avrest.start", aliases=['avrest.init'],description='Настройка бота, подробнее -help avrest.start')
@commands.has_permissions(administrator = True)
async def a_start(ctx, *choices: str):
    try:
        guild_type = choices[0] # ag/iso/...
        currency = choices[1] # ppt:0
        roles = choices[2] # HI-rank:0,H-rank:250,G-rank:1250,F-rank:6250,E-rank:32000,D-rank:160000,C-rank:800000,B-rank:4000000,A-rank:20000000,S-rank:10000000
        message_value = choices[3] # 1
        stocks = choices[4]# stocks:10000:5000:50:10000
        lott_tax = choices[5] # налог_лотереи (n чтобы отключить лоттерею)
        tax = choices[6] # 1_100,5_1000,15_10000,25_25000,30_50000,50_100000:1000 налог_перевода:переводы_без_налога
        is_auto = choices[7] # 1
    except:
        guild_type = choices[0] # ag/iso/...
        guild_key = choices[1] # public key
        is_auto = choices[2]
    cursor.execute("SELECT id FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
    u_id = cursor.fetchone()
    if u_id is not None:
        await ctx.reply("Гильдия уже была подключена, для перенастройки очистите данные (будут полностью утеряны все данные о гильдии и зарегестрированных пользователях) или обратитесь в поддержку.")
        return
    else:
        message = await ctx.reply("Подключение...")
        if (guild_type == 'ag'):
            await message.reply("Подключение к официальной гильдии [AG] успешно.")
            cursor.execute("INSERT INTO guilds_data (guild_id) VALUES (%i)" % (ctx.guild.id))
            cnx.commit()
            cursor.execute("SELECT id FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
            u_id = cursor.fetchone()
            u_id = u_id[0]
            add_employee = "UPDATE guilds_data SET g_privat_key = %s WHERE id = %s" 
            key_lok = secrets.token_urlsafe(64)
            param =(key_lok,u_id)
            cursor.execute(add_employee, param)
        elif (guild_type == 'iso'):
            await message.reply("Подключение изолированной гильдии успешно.")
            cursor.execute("INSERT INTO guilds_data (guild_id) VALUES (%i)" % (ctx.guild.id))
            cnx.commit()
            cursor.execute("SELECT id FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
            u_id = cursor.fetchone()
            u_id = u_id[0]
            c_con = currency.split(":")
            s_con = stocks.split(":")
            patterns = ('guild_type', 'is_master_guild', 'ppt_name', 'ppt_start', 'guild_ranks','per_message' , 'guild_taxs', 'lott_tax', 'stocks_name', 'stocks_buy', 'stocks_sell', 'stocks_key', 'stocks_at_all')
            data_employee = ('iso',1,c_con[0],c_con[1],roles,message_value,tax,lott_tax,s_con[0],s_con[1],s_con[2],s_con[3],s_con[4] )
            for i in range(len(patterns)):
                add_employee = (f"UPDATE guilds_data SET "+patterns[i]+"=%s WHERE id=%s")
                param =(str(data_employee[i]),u_id)               
                cursor.execute(add_employee, param)
            
            key_pub = secrets.token_urlsafe(32)
            key_lok = secrets.token_urlsafe(64)
            
            add_employee = "UPDATE guilds_data SET g_privat_key = %s WHERE id = %s" 
            param =(key_lok,u_id)
            cursor.execute(add_employee, param)
            add_employee = "UPDATE guilds_data SET g_public_key = %s WHERE id = %s" 
            param =(key_pub,u_id)
            cursor.execute(add_employee, param)
            tablename = str(ctx.guild.id)+'_users_data'
            cursor.execute("SELECT * FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
            u_id = cursor.fetchone()
            cursor.execute("CREATE TABLE `%s` ("
                "`id` bigint NOT NULL AUTO_INCREMENT,"
                "`user_id` bigint DEFAULT NULL,"
                "`date` datetime DEFAULT CURRENT_TIMESTAMP,"
                "`ppt` bigint DEFAULT '%i',"
                "`rank` tinyint DEFAULT '0',"
                "`karma` int DEFAULT '0',"
                "`stocks` int DEFAULT '0',"
                "`gem_one` int DEFAULT '0',"
                "`gem_two` int DEFAULT '0',"
                "`gem_three` int DEFAULT '0',"
                "`is_help` tinyint DEFAULT '0',"
                "`daily_limit` int DEFAULT '10000',"
                "`daily_presents` int DEFAULT '0',"
                "`boost` tinyint DEFAULT '1',"
                "`user_pass` varchar(64) DEFAULT NULL,"
                "`fiat` int DEFAULT '0',"
                "`user_status` varchar(256) DEFAULT NULL,"
                "`user_title` varchar(256) DEFAULT NULL,"
                "`user_key` varchar(96) DEFAULT NULL,"
                "`recomend` varchar(256) DEFAULT '0',"
                "`comment` varchar(2048) DEFAULT '0',"
                "`resume` varchar(256) DEFAULT '0',"
                "PRIMARY KEY (`id`),UNIQUE KEY `id_UNIQUE` (`id`),UNIQUE KEY `user_id_UNIQUE` (`user_id`)) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci" % (tablename,u_id[8]))
            cnx.commit()
            add_employee = "UPDATE guilds_data SET g_data = %s WHERE id = %s" 
            param = (tablename, u_id[0])
            cursor.execute(add_employee, param)   
            cnx.commit()     
        else:
            cursor.execute("SELECT * FROM guilds_data WHERE id = %s" % (guild_type))
            u_id = cursor.fetchone()
            if(u_id[0]==None):
                await message.reply("Не существующий ключ.")
                return
            if (u_id[26]!=guild_key):
                await message.reply("Код подключения не верный.")
                return
            await message.reply("Подключение к сторонней изолированной гильдии успешно.")
            cursor.execute("INSERT INTO guilds_data (guild_id) VALUES (%i)" % (ctx.guild.id))
            cnx.commit()
            cursor.execute("SELECT * FROM guilds_data WHERE guild_id = %s" % (ctx.guild.id))
            u_id = cursor.fetchone()
            u_id = u_id[0]
            cursor.execute("SELECT * FROM guilds_data WHERE id = %s" % (guild_type))
            u_id_p = cursor.fetchone()
            patterns = ['guild_type', 'is_master_guild', 'g_data', 'guild_ballance', 'g_prefix', 'ppt_name', 'ppt_start', 'guild_ranks','per_message', 'guild_taxs', 'is_lott_active', 'lott_tax', 'stocks_name', 'stocks_buy', 'stocks_sell', 'stocks_key', 'stocks_at_all','stocks_vol', 'status_tier', 'guild_gen_category', 'guild_gen_ch_name', 'rooms_category', 'rooms_preff', 'rooms_post', 'g_public_key','promo']
            data_employee = u_id_p[4:27]
            data_employee = ("un",0,)+data_employee+(u_id_p[-1],)
            for i in range(len(patterns)):
                add_employee = (f"UPDATE guilds_data SET "+patterns[i]+"=%s WHERE id=%s")
                param =(str(data_employee[i]),u_id)               
                cursor.execute(add_employee, param)
            key_lok = secrets.token_urlsafe(64)
            add_employee = "UPDATE guilds_data SET g_privat_key = %s WHERE id = %s" 
            param =(key_lok,u_id)
            cursor.execute(add_employee, param)
            cnx.commit()         
    if bool(int(is_auto)):
        r = ["muted",]
        r_gnt = roles.split(",")
        for i in r_gnt:
            q = i.split(":")
            r.append(q[0])
        for i in r:
            await ctx.guild.create_role(name=i)
        await ctx.guild.create_category("~[AG]~", overwrites=None, reason="auto generated by settup /")
        await ctx.guild.create_text_channel(name='all4', category="~[AG]~", overwrites=None)
        await ctx.guild.create_text_channel(name='commands', category="~[AG]~", overwrites=None)
        await ctx.guild.create_voice_channel("Create_Room", overwrites=None, category="~[AG]~", reason="auto generated by settup /")
        await ctx.guild.create_category("rooms", overwrites=None, reason="auto generated by settup /")
        await ctx.guild.create_category("afk", overwrites=None, reason="auto generated by settup /")
        await ctx.guild.create_voice_channel("afk", overwrites=None, category="afk", reason="auto generated by settup /")
        await ctx.author.send("Автоматическая настройка проведена.")
    await ctx.send("Настройки применены успешно.")

#cmp
@bot.command(name="choose", aliases=['chs'],description='Выбор случайного элемента, подробнее -help choose')
async def choose(ctx, *choices: str):
    await ctx.reply(random.choice(choices))

@bot.command(name="reg", aliases=['registration'], description="Регистрация/Восстановление в сервисах [AG], подробнее -help reg")
async def _reg(ctx):
    cursor.execute("SELECT g_data FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
    g_id = cursor.fetchone() 
    cursor.execute("SELECT id FROM %s WHERE user_id=%i" % (g_id[0],ctx.author.id))
    u_id = cursor.fetchone()  
    if u_id is not None:
        u_id = u_id[0]
        if(ctx.author.top_role.name =='@everyone'):
            cursor.execute("SELECT * FROM %s WHERE id=%i" % (g_id[0],u_id))
            rank = cursor.fetchone()
            rank = rank[4]
            cursor.execute("SELECT guild_ranks FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
            g_id = cursor.fetchone() 
            g_id = g_id[0]
            g_id = g_id.split(",")
            gg_id = []
            for i in g_id: 
                t = i.split(":")   
                gg_id.append(t[0])
            if (rank>len(gg_id)):
                await ctx.send("Ошибка r-0 обратитесь к администратору сервера.")
                return
            await ctx.author.add_roles(discord.utils.get(ctx.author.guild.roles, name=gg_id[rank]))
            await ctx.send(f'{ctx.message.author.mention} Восстановление успешно.')
        else:
            await ctx.send(f'{ctx.message.author.mention} регистрация/восставновление не требуются.')
    else:
        cursor.execute("INSERT INTO %s (user_id) VALUES (%i)" % (g_id[0],ctx.author.id))
        cnx.commit()
        cursor.execute("SELECT guild_ranks FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
        g_id = cursor.fetchone() 
        g_id = g_id[0]
        g_id = g_id.split(",")
        g_id = g_id[0]
        g_id = g_id.split(":")
        g_id = g_id[0]
        await ctx.author.add_roles(discord.utils.get(ctx.author.guild.roles, name=g_id))
        await ctx.send(f'{ctx.message.author.mention} регистрация пройдена успешно.')

@bot.command(name="balance", aliases=['blnc', 'ppt'], description="Узнать баланс, подробнее -help balance")
async def balance(ctx, tmp = None):
    cursor.execute("SELECT * FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
    g_id = cursor.fetchone() 
    cursor.execute("SELECT ppt FROM %s WHERE user_id = %i" %(g_id[4],ctx.author.id))
    result = cursor.fetchone()
    s = str(result[0])
    if (tmp == "public")or(tmp == "pub")or(tmp == "p"):   
        await ctx.reply('Баланс {'+s+'}'+f' {g_id[7]}')
    else:
        await ctx.author.send('Баланс {'+s+'}'+f' {g_id[7]}')
    
@bot.command(name="give", aliases=['giveppt', 'gv'], description="Перевести пользователю PPt (облагается налогом, подробнее -help give)")
async def _giveppt(ctx,member : discord.Member, pptt : int):
    cursor.execute("SELECT * FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
    g_id = cursor.fetchone() 
    cursor.execute("SELECT * FROM %s WHERE user_id = %i" %(g_id[4],ctx.author.id))
    mess_auth = cursor.fetchone()
    cursor.execute("SELECT * FROM %s WHERE user_id = %i" %(g_id[4],member.id))
    mess_memb = cursor.fetchone()
    if (mess_auth[0]==mess_memb[0]):
        ctx.author.send("Совпадение субъектов.")
        return
    if (mess_auth[0]==None)or(mess_memb[0]==None):
        ctx.author.send("Невозможно найти ячейку данных.")
        return
    pptt = int(pptt)
    try:
        nal_temp = g_id[11].split(":")
        presents = int(nal_temp[1])
    except:
        pass
    pptttoday = mess_auth[11]
    if (pptt>=1):
        if (mess_auth[3]>=pptt):
            if g_id[11]==-1:
                temp = pptt
            else:
                ppt_at_all = pptttoday+pptt
                if ppt_at_all >= presents:
                    presents_o = presents-pptttoday
                    if presents_o>0:
                        summa = pptt-presents_o
                    else:
                        presents_o = 0
                        summa = pptt
                    temp = 0
                    nal = []
                    nal_e = []
                    nal_temp =  nal_temp[0].split(",")
                    for k in nal_temp:
                        y = k.split("_")
                        nal.append(int(y[0])) 
                        nal_e.append(int(y[1]))
                    for i in range(len(nal)):
                        if pptttoday<nal_e[i]:
                            percent = i
                            break
                    else:
                        percent = len(nal)-1        
                    for i in range(percent,len(nal)):
                        if (summa<nal_e[i]):
                            temp+=summa*(1-0.01*nal[i])
                            break
                        else:
                            temp+=nal_e[i]*(1-0.01*nal[i])
                            summa-=nal_e[i] 
                else:
                    temp=pptt
                    presents_o=None 
            if presents_o!=None:
                temp += presents_o          
            cursor.execute("UPDATE %s SET ppt = %i WHERE id = %i" % (g_id[4],mess_auth[3]-pptt, mess_auth[0]))
            cursor.execute("UPDATE %s SET daily_limit = %i WHERE id = %i" % (g_id[4],mess_auth[11]+pptt, mess_auth[0]))
            cursor.execute("UPDATE %s SET ppt = %i WHERE id = %i" % (g_id[4],mess_memb[3]+temp, mess_memb[0]))
            cursor.execute("UPDATE guilds_data SET guild_ballance = %i WHERE guild_id = %i" % (g_id[5]+(pptt-temp), ctx.guild.id))
            cnx.commit()
            await ctx.reply('Успешно')
            await ctx.author.send('Вы перевели {'+str(pptt)+'} PPt пользователю '+str(member)+' налог {'+str(int(pptt-temp))+'}')
            await member.send('Вы получили {'+str(temp)+'} PPt от пользователя ' +str(ctx.author.name)+' с вычетом налога {'+str(int(pptt-temp))+'}')
        else:
            await ctx.send('Недостаточно PPt.')
    else:
        await ctx.send('...')    
    


    
@bot.command(name="create.ticket", aliases=['crttckt'], description="Создать код (купон) определенного типа, подробнее -help create.ticket")
async def _create_ticket(ctx, type, cell, amount, times, code):
    # -create.ticket set/add ppt/karma/ 1/10/ 1/10/0 code/n
    cursor.execute("SELECT * FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
    g_id = cursor.fetchone() 
    if (g_id[2]!='iso')and(ctx.author.id!=711150427063975997):
        return
    cursor.execute("SELECT * FROM %s WHERE user_id = %i" %(g_id[4],ctx.author.id))
    mess_auth = cursor.fetchone()
    if (mess_auth[10]==0):
        await ctx.send("Недостаточно прав.")
        return
    await ctx.send("Успешно.")
    if (code == 'n'):
        rand = randint(0,999999999999)
        code = generate(6, '-', 3, 7, type_of_value = 'hex', capital = 'mix', extras = ['%', '&', '^'], seed = rand).get_key()
    last_Row = len(list(filter(None, db_codes.col_values(1)))) + 1
    db_codes.update_cell(last_Row, 1, code)
    db_codes.update_cell(last_Row, 2, type)
    db_codes.update_cell(last_Row, 3, cell)
    db_codes.update_cell(last_Row, 4, amount)
    db_codes.update_cell(last_Row, 5, times)
    db_codes.update_cell(last_Row, 6, "auth-" + str(ctx.author.id))
    db_codes.update_cell(last_Row, 7, g_id[4])
    db_codes.update_cell(last_Row, 8, "~")
    await ctx.author.send("code: "+str(code))

#проверить
@bot.command(name="use.ticket", aliases=['stckt'], description="Использовать код (купон), подробнее -help use.ticket")
async def use_ticket(ctx, key):
    cursor.execute("SELECT * FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
    g_id = cursor.fetchone()
    cell = db_codes.find(key)
    if (cell==None):
        await ctx.author.send("Код/купон не найден.")
        return
    code_c = db_codes.row_values(cell.row)
    if (g_id[4] != code_c[6])and(code_c[6]!="avrest.univers"):
        ctx.author.send("Не возможно использовать код на данном сервере.")
        return
    if (len(code_c[7])<=int(code_c[4])) or (code_c[4]=='0'):
        users_s = code_c[7].split(",")
        if str(ctx.author.id) in users_s:
            await ctx.send("Вы уже использовали этот код")
            return
        else:
            await ctx.send("Успешно.")
            users_s.append(str(ctx.author.id))
            users_c = ",".join(users_s)
            db_codes.update_cell(cell.row, 8, users_c)
            cursor.execute("SELECT * FROM %s WHERE user_id = %i" %(g_id[4], ctx.author.id))
            mess_auth = cursor.fetchone()
            if (code_c[1]=='set'):
                cursor.execute("UPDATE %s SET %s = %i WHERE id = %i" % (g_id[4], code_c[2], int(code_c[3]), mess_auth[0]))
                cnx.commit()
            elif (code_c[1]=='add'):
                cursor.execute("SELECT %s FROM %s WHERE user_id = %i" %(code_c[2], g_id[4], ctx.author.id))
                some = cursor.fetchone()
                cursor.execute("UPDATE %s SET %s = %i WHERE id = %i" % (g_id[4], code_c[2], int(some[0])+int(code_c[3]), mess_auth[0]))
                cnx.commit()
            else:
                return  
    else:
        await ctx.send("Превышено количество использований.")

@bot.command(name="rank.up", aliases=['rnkp'], description="Повысить ранг, подробнее -help rank.up")
async def _rank(ctx):
    cursor.execute("SELECT * FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
    g_id = cursor.fetchone()
    cursor.execute("SELECT * FROM %s WHERE user_id = %i" %(g_id[4],ctx.author.id))
    mess_auth = cursor.fetchone()
    rank_to_index = {}
    index_to_rank = {}
    nal_temp =  g_id[9].split(",")
    for i in range(len(nal_temp)):
        y = nal_temp[i].split(":")
        rank_to_index[y[0]] = i
        index_to_rank[i] = y[0],y[1]
    top_role = ctx.author.top_role.name  
    if top_role == "@everyone":
        await ctx.author.edit(roles=[])
        await ctx.author.add_roles(discord.utils.get(ctx.author.guild.roles, name=index_to_rank[0][0]))
        await ctx.send("Вы получили базовый ранг.")
        return  
    for i in index_to_rank:
        if index_to_rank[i][0] == top_role:
            if rank_to_index[top_role]!=mess_auth[4]:
                await ctx.author.edit(roles=[])
                await ctx.author.add_roles(discord.utils.get(ctx.author.guild.roles, name=index_to_rank[mess_auth[4]][0]))
                await ctx.send("Ранг успешно восстановлен.")
                return          
            else:
                current_rank = rank_to_index[top_role]
                if (current_rank==len(rank_to_index)-1):
                        await ctx.author.send("Вы уже достигли максимально возможного ранга ^-^")
                        return
                if g_id[29]==1:
                    recom = mess_auth[19].split("~")
                    if((current_rank==len(rank_to_index)-2)and(len(recom)<3)or((current_rank==len(rank_to_index)-3)and(len(recom)<1))):
                        await ctx.author.send("Вы собрали недостаточно рекомендаций.")
                        return  
                if (mess_auth[3]>=int(index_to_rank[i+1][1])):
                    await ctx.send(f"{ctx.message.author.mention} повышен до {index_to_rank[i+1][0]}.")
                    cursor.execute("UPDATE %s SET ppt = %i WHERE id = %i" % (g_id[4], int(mess_auth[3])-int(index_to_rank[i+1][1]), mess_auth[0]))
                    rankk = i+1
                    cursor.execute("UPDATE `%s` SET `rank` = %i WHERE `id` = %i" % (g_id[4], rankk, mess_auth[0]))
                    cnx.commit()
                    await ctx.author.remove_roles(discord.utils.get(ctx.author.guild.roles, name=index_to_rank[i][0]))
                    await ctx.author.add_roles(discord.utils.get(ctx.author.guild.roles, name=index_to_rank[i+1][0]))
                else:
                    await ctx.send(f"До следующего ранга {int(index_to_rank[i+1][1])-mess_auth[3]} {g_id[7]}")

@bot.command(name="rank.info", aliases=['rnknf'], description="Информация о повышении ранга, подробнее -help rank.info")
async def _rankinfo(ctx):
    cursor.execute("SELECT * FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
    g_id = cursor.fetchone()
    embed = discord.Embed(color = 0xff9900, title = f'All Ranks {ctx.guild.name}') # Создание Embed'a
    nal_temp =  g_id[9].split(",")
    for i in nal_temp:
        y = i.split(":")
        if y[1]==0:
            embed.add_field(name=y[0], value="Выдается при регистрации -reg.", inline=False)
        elif(g_id[29]==1)and(i==nal_temp[-1]):
            embed.add_field(name=y[0], value=f"{y[1]} {g_id[7]} и 3 рекомендации", inline=False)
        elif(g_id[29]==1)and((i==nal_temp[-2])):
            embed.add_field(name=y[0], value=f"{y[1]} {g_id[7]} и 1 рекомендация", inline=False)
        else:
            embed.add_field(name=y[0], value=f"{y[1]} {g_id[7]}", inline=False)
    if g_id[2]=='ag':
        embed.set_footer(text="Ранги сохраняются и переносятся на ВСЕ гильдии AG, все ранги являются постоянными, НО [S-rank] является временным, ваш ранг/PPt могут понизить/изъять при нарушении правил сообществ AG.")
    else:
        embed.set_footer(text="Ранги сохраняются и переносятся на ВСЕ связанные гильдии, ранги являются постоянными, однако могут быть изменены модераторами/админами/поддержкой.")
    await ctx.author.send(embed = embed) # Отправляем Embed

@bot.command(name="solve", aliases=['slv'], description="Решить произвольную задачу (любой тип), подробнее -help solve")
async def solve(ctx, *query : str):
    query = ' '.join(query)
    res = client.query(query)
    embed=discord.Embed(title=str(query), url='', description=' ', color=0xFF5733)
    for pod in res.pods:
        embed.add_field(name=str(pod.title), value=str(pod.text), inline=False)
    await ctx.author.send(embed=embed)

@bot.command(name="search", aliases=['srch'], description="Узнать информацию (эталон), подробнее -help search")
async def search(ctx, *query : str):
    query = ' '.join(query)
    wikipedia.set_lang("ru")
    ny = wikipedia.page(query)
    embed=discord.Embed(title=ny.title, url=ny.url, description=wikipedia.summary(str(query), sentences=5), color=0xFF5733)
    await ctx.author.send(embed=embed)

@bot.command(name="search.eng", aliases=['srchng','srchn'], description="Узнать информацию англоязычный поиск (эталон), подробнее -help search")
async def searcheng(ctx, *query : str):
    query = ' '.join(query)
    wikipedia.set_lang("en")
    ny = wikipedia.page(query)
    embed=discord.Embed(title=ny.title, url=ny.url, description=wikipedia.summary(str(query), sentences=5), color=0xFF5733)
    await ctx.author.send(embed=embed)

@bot.command(name="lott", aliases=['ltt'], description="Поставить некоторое количество PPt (все зависит от вашей удачи), подробнее -help lott")
async def _lott(ctx, amount, num=None):
    cursor.execute("SELECT * FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
    g_id = cursor.fetchone()
    cursor.execute("SELECT * FROM %s WHERE user_id = %i" %(g_id[4],ctx.author.id))
    mess_auth = cursor.fetchone()
    if (amount=='all'):
        num = int(mess_auth[3])
    elif (amount=='half'):
        num = int(int(mess_auth[3])/2)
    elif (amount=='quarter'):
        num = int(int(mess_auth[3])/4)
    elif (amount=='quaver'):
        num = int(int(mess_auth[3])/8)
    else:
        num= abs(int(num))
    if (mess_auth[3]<num):
        await ctx.send("Недостаточно ppt.")
        return
    if (1<=num<=10000):
        res = randint(-num,num)
        if (res == 0):
            res = 1000
    elif(10000<num):
        res = randint(-num,num)
        if (res == 0):
            res = 100000
    else:
        await ctx.send("Ошибка Null суммы")
        return 
    cursor.execute("UPDATE %s SET ppt = %i WHERE id = %i" % (g_id[4], int(mess_auth[3])+res, mess_auth[0]))
    cnx.commit()
    if(res>0):
        await ctx.send(f"Вы поставили {num} PPt, получили {res} PPt!")
    else:
        await ctx.send(f"Вы поставили {num} PPt, потеряли {res} PPt!")

@bot.command(name="stocks.buy", aliases=['stcksb'], description="Купить токен активности сервера, подробнее -help stocks.buy")
async def _buystocks(ctx,count):
    cursor.execute("SELECT * FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
    g_id = cursor.fetchone()
    cursor.execute("SELECT * FROM %s WHERE user_id = %i" %(g_id[4],ctx.author.id))
    mess_auth = cursor.fetchone() 
    count=int(count)
    if (count>=1):
        if (mess_auth[6]+count<=g_id[19]):
            if (mess_auth[3]>=g_id[15]*count):
                if (g_id[18]>=count):
                    cursor.execute("UPDATE %s SET ppt = %i WHERE id = %i" % (g_id[4], mess_auth[3]-g_id[15]*count, mess_auth[0]))
                    cursor.execute("UPDATE %s SET stocks = %i WHERE id = %i" % (g_id[4], mess_auth[6]+count, mess_auth[0]))
                    cursor.execute("UPDATE guilds_data SET stocks_at_all = %i WHERE id = %i" % (g_id[18]-count, g_id[0]))
                    cnx.commit()
                    await ctx.send('Успешно')
                else:
                    await ctx.send('На бирже недостаточно токенов. Доступно {'+(g_id[18])+'}')
            else:
                await ctx.send('Недостаточно PPt.')
        else:
            await ctx.send(f"Один пользователь может иметь не более {g_id[15]} токенов.")
    else:
        await ctx.send("... Умно... Наверное...")

@bot.command(name="stocks.sell", aliases=['stckssll'], description="Продать токен активности сервера, подробнее -help stocks.sell")      
async def _sellstocks(ctx,count):
    cursor.execute("SELECT * FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
    g_id = cursor.fetchone()
    cursor.execute("SELECT * FROM %s WHERE user_id = %i" %(g_id[4], ctx.author.id))
    mess_auth = cursor.fetchone() 
    count = int(count)
    if (count>=1):
        if (mess_auth[6]>=count):
            cursor.execute("UPDATE %s SET ppt = %i WHERE id = %i" % (g_id[4], mess_auth[3]+g_id[16]*count, mess_auth[0]))
            cursor.execute("UPDATE %s SET stocks = %i WHERE id = %i" % (g_id[4], mess_auth[6]-count, mess_auth[0]))
            cursor.execute("UPDATE guilds_data SET stocks_awail = %i WHERE id = %i" % (g_id[18]+count, g_id[0]))
            cnx.commit()
            await ctx.author.send('Успешно.')   
        else:
            await ctx.author.send('Недостаточно токенов PPt.')
    else:
        await ctx.send("... Умно... Наверное...")        
        
@bot.command(name="stocks", aliases=['stcks'], description="Информация о токенах активности сервера, подробнее -help stocks")       
async def _stocks(ctx):
    cursor.execute("SELECT * FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
    g_id = cursor.fetchone()
    cursor.execute("SELECT * FROM %s WHERE user_id = %i" %(g_id[4], ctx.author.id))
    mess_auth = cursor.fetchone() 
    embed = discord.Embed(color = 0x9863E6, title =f"{g_id[14]}") # Создание Embed'a
    embed.add_field(name="Баланс : ", value=f"{mess_auth[6]} {g_id[14]}", inline=True)
    embed.add_field(name="Доход 1 токена за Р.П: ", value=str(g_id[26]), inline=True)
    embed.add_field(name="Доход за Р.П: ", value=str((int(g_id[26])*mess_auth[6])), inline=True)
    embed.set_footer(text=f'Р.П. - расчетный период, обновляется в конце каждого дня, значения доходов указаны в {g_id[7]}. '+'Ключевая ставка:{'+str(g_id[17])+'}'+f' {g_id[7]}.')
    await ctx.author.send(embed = embed) # Отправляем Embed        

@bot.command(name="report", aliases=['rep','rprt'], description="Сообщить о нарушении правил/о баге, подробнее -help report")
async def _report(ctx, *s : str):
    s = ' '.join(s)
    cursor.execute("SELECT * FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
    g_id = cursor.fetchone()
    q = 711150427063975997
    user = await bot.fetch_user(q)
    embed = discord.Embed(color = 0xff9900, title = f'Report запрос.') # Создание Embed'a
    embed.add_field(name=f"{ctx.author.name}:{ctx.author.id}", value=s, inline=False)
    embed.set_footer(text=f"{ctx.guild.name}:{ctx.guild.id}:{ctx.author.name}:{ctx.author.discriminator}")
    await ctx.message.delete()
    await user.send(embed = embed)
    await ctx.author.send("Обращение принято в рассмотрение.")

@bot.command(name="clear", description="Очистить несколько последних сообщений, подробнее -help clear")
@commands.has_permissions(administrator = True)
async def clear(ctx, amount=None):
    await ctx.channel.purge(limit=int(amount)+1)
    await ctx.author.send('Сообщения успешно удалены {'+amount+'}')

@bot.command(name="recomend",aliases=['rcmnd'] , description="Рекомендовать пользователя на повышение ранга, подробнее -help recomend")
async def _recomend(ctx, member : discord.Member):
    cursor.execute("SELECT * FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
    g_id = cursor.fetchone()
    if g_id[29]==0:
        ctx.author.send("Функция отключена модератором.")
        return
    cursor.execute("SELECT * FROM %s WHERE user_id = %i" %(g_id[4], ctx.author.id))
    mess_auth = cursor.fetchone()
    tmp = []
    nal_temp =  g_id[9].split(",")
    for i in nal_temp:
        y = i.split(":")
        tmp.append(y[1])    
    if (mess_auth[4]>=len(nal_temp)-2):
        if (mess_auth[3]>=(int(tmp[-2]/2))):
            await ctx.send("Успешно.")
            cursor.execute("UPDATE %s SET ppt = %i WHERE id = %i" % (g_id[4],mess_auth[3]-int(tmp[-2]/2), mess_auth[0]))
            cnx.commit()
            aut = str(ctx.author.id)
            mem = str(member.id)
            if (aut!=mem):
                cursor.execute("SELECT * FROM %s WHERE user_id = %i" %(g_id[4], member.id))
                mess_memb = cursor.fetchone()
                q = mess_memb[0][19]
                arr = list(map(str, q.split("~")))
                chec = 0   
                for l in arr:
                    if (l==aut):
                        chec = 1
                if (chec == 0):
                    arr.append(f"{ctx.author.id}")
                    q = "~".join(arr)
                    cursor.execute("UPDATE %s SET recomend = %i WHERE id = %i" % (g_id[4],q , mess_memb[0]))
                    cnx.commit()
                    await ctx.send("Ваш голос принят.")
                else: 
                    await ctx.send("Ваш голос уже был учтен.") # Отправляем Embed
            else:
                await ctx.send(f"Голос за себя не может быть не засчитан, {g_id[7]} не подлежат возврату.")
        else:
            ctx.send(f"Недостаточно {g_id[7]}.")
    else:
        await ctx.send("Рекомендации могут оставлять только пользователи максимального ранга.")

@commands.cooldown(1, 300, commands.BucketType.user)
@bot.command(name="stream",aliases=['streamer','strm','strmr'], description="Получить права вести стримы и трансляции, если их нет по умолчанию, подробнее -help stream")
async def _join_to(ctx):
    try:
        role = discord.utils.get(ctx.author.guild.roles, name="stream")
    except:
        await ctx.author.send("Невозможно выдать роль.")
        return
    await ctx.author.add_roles(role)
    await asyncio.sleep(300)
    await ctx.author.remove_roles(role)



@bot.command(name="join.to", description="Перейти к каналу пользователя, подробнее -help join.to")
async def _join_to(ctx, member : discord.Member):
    try:
        cursor.execute("SELECT * FROM guilds_data WHERE guild_id=%i" % (ctx.guild.id))
        g_id = cursor.fetchone()
        cursor.execute("SELECT * FROM %s WHERE user_id = %i" %(g_id[4],member.id))
        mess_auth = cursor.fetchone()
        await ctx.author.move_to(await get_channel_by_id(ctx.guild,mess_auth[30]))
    except:
        ctx.author.send("Ошибка присоединения по ключу.")
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#Создание комнат
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
async def create_voice_channel(guild,channel_name,category_name,user_limit=None):
    category = await get_category_by_name(guild,category_name)
    await guild.create_voice_channel(channel_name,category=category,user_limit = user_limit)
    channel = await get_channel_by_name(guild,channel_name)
    return channel

async def get_channel_by_name(guild,channel_name):
    channel = None
    for c in guild.channels:
        if c.name == channel_name.lower():
            return c
    return channel

async def get_channel_by_id(guild,channel_id):
    channel = None
    for c in guild.channels:
        if c.id == channel_id:
            return c
    return channel

async def get_category_by_name(guild,category_name):
    category = None
    for c in guild.categories:
        if c.name == category_name:
            return c
    return category 

class Events(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        q = 711150427063975997
        user = await bot.fetch_user(q)
        await user.send("Restarted")

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.author.bot: return
        try:
            cursor.execute("SELECT * FROM guilds_data WHERE guild_id=%i" % (message.guild.id))
            g_id = cursor.fetchone()
            cursor.execute("SELECT * FROM %s WHERE user_id = %i" %(g_id[4],message.author.id))
            mess_auth = cursor.fetchone()
            if mess_auth == None:
                cursor.execute("INSERT INTO %s (user_id) VALUES (%i)" % (g_id[4],message.author.id))
                cnx.commit()
                cursor.execute("SELECT * FROM %s WHERE user_id = %i" %(g_id[4],message.author.id))
                mess_auth = cursor.fetchone()
                cursor.execute("SELECT guild_ranks FROM guilds_data WHERE guild_id=%i" % (message.guild.id))
                g_id_ = cursor.fetchone() 
                g_id_ = g_id_[0]
                g_id_ = g_id_.split(",")
                g_id_ = g_id_[0]
                g_id_ = g_id_.split(":")
                g_id_ = g_id_[0]
                await message.author.add_roles(discord.utils.get(message.author.guild.roles, name=g_id_)) 
            cursor.execute("UPDATE %s SET ppt = %i WHERE id = %i" % (g_id[4],mess_auth[3]+g_id[10], mess_auth[0]))
            cnx.commit()
        except:
            pass

    @commands.Cog.listener()
    async def on_voice_state_update(self,member,before,after):
        if member.bot:
            return
        
        if after.channel is not None:
            cursor.execute("SELECT * FROM guilds_data WHERE guild_id=%i" % (after.channel.guild.id))
            g_id = cursor.fetchone()
            cursor.execute("SELECT * FROM %s WHERE user_id = %i" %(g_id[4],member.id))
            mess_auth = cursor.fetchone()
            cursor.execute("UPDATE %s SET ppt = %i WHERE id = %i" % (g_id[4],mess_auth[3]+g_id[10]*5, mess_auth[0]))
            cnx.commit()
            channel = None
            cursor.execute("UPDATE %s SET user_channel = %s WHERE id = %i" % (g_id[4],after.channel.id, mess_auth[0]))
            if after.channel.name == g_id[22]:
                if before.channel is not None:
                    cat = await get_category_by_name(before.channel.guild,g_id[23])
                    if before.channel.category.id == cat.id:
                        if len(before.channel.members) == 0:
                            await before.channel.delete()
                preff = g_id[24]
                postf = g_id[25]
                if preff == None:
                    preff = ""
                if postf== None:
                    postf = ""
                elif postf == "descr":
                    postf = member.discriminator
                    
                channel = await create_voice_channel(after.channel.guild,f'{preff}{member.name}{postf}'.lower(),g_id[23])
            
            if channel is not None:
                cursor.execute("UPDATE %s SET user_channel = %s WHERE id = %i" % (g_id[4],channel.id, mess_auth[0]))
                await member.move_to(channel)
        
        if before.channel is not None:
            cursor.execute("SELECT * FROM guilds_data WHERE guild_id=%i" % (before.channel.guild.id))
            g_id = cursor.fetchone()
            cat = await get_category_by_name(before.channel.guild,g_id[23])
            if before.channel.category.id == cat.id:
                if len(before.channel.members) == 0:
                    await before.channel.delete()

bot.add_cog(Events(bot))
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

@bot.command(name="avrest.core", description="Интерфейс команд Core, подробнее -help avrest.core")
@commands.has_permissions(administrator = True)
async def _join_to(ctx, core_comma, set_to = None):
    cursor.execute("SELECT * FROM guilds_data WHERE guild_id = %i" % (ctx.guild.id))
    g_id = cursor.fetchone()
    if (core_comma=='bot.set.prefix'):
        cursor.execute("UPDATE guilds_data SET g_prefix = ? WHERE id = ?" , (set_to, g_id[0]))
        await ctx.author.send(f"Guild prefix changed to {set_to}")
    elif(core_comma=='bot.set.channel.prefix'):
        cursor.execute("UPDATE guilds_data SET rooms_preff = ? WHERE id = ?" , (set_to, g_id[0]))
        await ctx.author.send(f"Guild channel prefix changed to {set_to}")
    elif(core_comma=='bot.set.channel.postfix'):
        cursor.execute("UPDATE guilds_data SET rooms_post = ? WHERE id = ?" , (set_to, g_id[0]))
        await ctx.author.send(f"Guild channel postfix changed to {set_to}")
    elif(core_comma=='bot.set.message.value'):
        cursor.execute("UPDATE guilds_data SET per_message = ? WHERE id = ?" , (set_to, g_id[0]))
        await ctx.author.send(f"Guild message value changed to {set_to}")
    elif(core_comma=='bot.set.currency.name'):
        cursor.execute("UPDATE guilds_data SET ppt_name = ? WHERE id = ?" , (set_to, g_id[0]))
        await ctx.author.send(f"Guild currency name changed to {set_to}")
    elif(core_comma=='bot.set.guild.rangs'):
        cursor.execute("UPDATE guilds_data SET guild_ranks = ? WHERE id = ?" , (set_to, g_id[0]))
        await ctx.author.send(f"Guild rangs changed to {set_to}")
    elif(core_comma=='bot.set.guild.taxs'):
        cursor.execute("UPDATE guilds_data SET guild_taxs = ? WHERE id = ?" , (set_to, g_id[0]))
        await ctx.author.send(f"Guild taxs changed to {set_to}")
    elif(core_comma=='bot.set.lott.activity'):
        cursor.execute("UPDATE guilds_data SET is_lott_active = ? WHERE id = ?" , (set_to, g_id[0]))
        await ctx.author.send(f"Guild lott activity changed to {set_to}")
    elif(core_comma=='bot.set.lott.tax'):
        cursor.execute("UPDATE guilds_data SET lott_tax = ? WHERE id = ?" , (set_to, g_id[0]))
        await ctx.author.send(f"Guild lott tax changed to {set_to}")
    elif(core_comma=='bot.set.stocks.name'):
        cursor.execute("UPDATE guilds_data SET stocks_name = ? WHERE id = ?" , (set_to, g_id[0]))
        await ctx.author.send(f"Guild stocks name changed to {set_to}")
    elif(core_comma=='bot.set.stocks.buy'):
        cursor.execute("UPDATE guilds_data SET stocks_buy = ? WHERE id = ?" , (set_to, g_id[0]))
        await ctx.author.send(f"Guild stocks buy changed to {set_to}")
    elif(core_comma=='bot.set.stocks.sell'):
        cursor.execute("UPDATE guilds_data SET stocks_sell = ? WHERE id = ?" , (set_to, g_id[0]))
        await ctx.author.send(f"Guild stocks sell changed to {set_to}")
    elif(core_comma=='bot.set.stocks.key'):
        cursor.execute("UPDATE guilds_data SET stocks_key = ? WHERE id = ?" , (set_to, g_id[0]))
        await ctx.author.send(f"Guild stocks key changed to {set_to}")
    elif(core_comma=='bot.set.stocks.number'):
        cursor.execute("UPDATE guilds_data SET stocks_at_all = ? WHERE id = ?" , (set_to, g_id[0]))
        await ctx.author.send(f"Guild stocks number changed to {set_to}")
    elif(core_comma=='bot.set.rang.promote'):
        cursor.execute("UPDATE guilds_data SET promo = ? WHERE id = ?" , (set_to, g_id[0]))
        await ctx.author.send(f"Guild rang promote changed to {set_to}")
    elif(core_comma=='bot.set.status.tear'):
        cursor.execute("UPDATE guilds_data SET status_tier = ? WHERE id = ?" , (set_to, g_id[0]))
        await ctx.author.send(f"Guild status tear changed to {set_to}")
    elif(core_comma=='bot.set.main.channel.category.name'):
        cursor.execute("UPDATE guilds_data SET guild_gen_category = ? WHERE id = ?" , (set_to, g_id[0]))
        await ctx.author.send(f"Guild main voice category name changed to {set_to}")
    elif(core_comma=='bot.set.channel.category.name'):
        cursor.execute("UPDATE guilds_data SET rooms_category = ? WHERE id = ?" , (set_to, g_id[0]))
        await ctx.author.send(f"Guild voice category name changed to {set_to}")
    elif(core_comma=='bot.set.main.channel.name'):
        cursor.execute("UPDATE guilds_data SET guild_gen_ch_name = ? WHERE id = ?" , (set_to, g_id[0]))
        await ctx.author.send(f"Guild main voice channel name changed to {set_to}")
    else:
        await ctx.author.send("Core command is not Exist")
        return
        


###########################################################
#tasks event_on bot
###########################################################
@tasks.loop(seconds=15)
async def refr_on_15_s_onbot():
    is_bool = randint(0,99)
    terms = ["OwO!","(＃￣0￣)","┐(￣ヘ￣)┌","٩(ˊ〇ˋ*)و","ヽ(°□° )ノ"]
    if is_bool>75:
        await bot.change_presence(status=discord.Status.idle, activity=discord.Game(random.choice(terms)))
    #logs_ch = bot.get_channel(avrest_const['log_ch'])
    #await logs_ch.send("Базы обновлены.")

@refr_on_15_s_onbot.before_loop
async def before():
    await bot.wait_until_ready()

refr_on_15_s_onbot.start()

###########################################################
#event_on bot
###########################################################
@bot.event
async def cach_errors(ctx, errors):
    pass



bot.run(avrest_const['token']) # Обращаемся к словарю settings с ключом token, для получения токена


#Avrest_API info

#guilds data
# 0,    1    ,     2     ,      3         ,   4   ,      5        ,    6     ,   7    ,    8     ,      9     ,    10     ,     11        ,    12   ,    13      ,    14     ,     15     ,     16    ,      17     ,        18      ,    19    ,     20       ,         21     ,        22      ,       23         ,     24    ,    25       , 26      ,     27       , 28 ,  29  ,     30        
#id, guild_id, guild_type, is_master_guild, g_data, guild_ballance, g_prefix, ppt_name, ppt_start, guild_ranks, per_message,guild_taxs, is_lott_active, lott_tax, stocks_name, stocks_buy, stocks_sell, stocks_key, stocks_at_all, stocks_vol, status_tier, guild_gen_category, guild_gen_ch_name, rooms_category, rooms_preff, rooms_post, g_public_key, g_privat_key, date, promo, current_channel

#users data
# 0,    1   ,   2 ,  3 ,  4  ,  5   ,   6   ,   7    ,   8    ,    9     ,   10   ,    11      ,    12         ,  13  ,   14     ,  15 ,     16     ,   17      ,     18  ,    19   ,    20    
#id, user_id, date, ppt, rank, karma, stocks, gem_one, gem_two, gem_three, is_help, daily_limit, daily_presents, boost, user_pass, fiat, user_status, user_title, user_key, recomend, comment
