#Утилиты
from imghdr import what
from platform import win32_edition
from re import A
import wikipedia
import wolframalpha
#Ключи
from key_generator.key_generator import generate
import mariadb
import secrets
#NC
from typing import Optional, Literal, Union, NamedTuple,List
from enum import Enum
import discord
from discord.ext import commands,tasks
from discord import Activity, Client, Guild, Interaction, app_commands
#other
import asyncio
import random
from random import choices, randint
from datetime import timezone, datetime, time
#json
import json

with open('avrest/config.json', 'r') as avrest_const:
    avrest_const = json.load(avrest_const)

avrest_current_version = avrest_const["version"]

client_wolf = wolframalpha.Client(avrest_const['wolphtok'])

cnx = mariadb.connect(user=avrest_const["db"]["login"], password=avrest_const["db"]["passwd"],host=avrest_const["db"]["host"],port=int(avrest_const["db"]["port"]),database=avrest_const["db"]["database"])

cnx.autocommit = True
cursor = cnx.cursor()

MY_GUILD = discord.Object(id=937925968704729131)
avrest_currences_list = [["Universal points","UNI"],]
avrest_pools_list = [["Avrest","1"],]
choice_avrest_currences_list = [app_commands.Choice(name="default", value="0")]+[app_commands.Choice(name=choice[0], value=choice[1]) for choice in avrest_currences_list]
choice_avrest_pool_list = [app_commands.Choice(name="default", value="0")]+[app_commands.Choice(name=choice[0], value=choice[1]) for choice in avrest_pools_list]
superior_temp = {}
superior_pool = {}
class MyClient(discord.Client):
    def __init__(self, *, intents: discord.Intents, status: discord.Status,activity :discord.Activity):
        super().__init__(intents=intents,status=status,activity=activity)
        self.tree = app_commands.CommandTree(self)

    async def setup_hook(self):
        ...
    
    async def guild_refresh(self,guild):
        print(guild.id)
        self.tree.copy_global_to(guild=guild)
        await self.tree.sync(guild=guild)


intents = discord.Intents.default()
status = discord.Status.idle
activity = discord.Activity(type=discord.ActivityType.competing ,name="procrastination",details="AG.presence.current.ai.machine") 
client = MyClient(intents=intents, status=status,activity=activity)

@client.tree.error
async def on_error_do(interaction: discord.Interaction, error):
    await interaction.user.send(f"```{error}```")
    pass    

@client.event
async def on_ready():
    for guild in client.guilds:
        await client.guild_refresh(guild)
    global report_channel
    report_channel = client.get_channel(1021855625740828752) 
    print(report_channel.name)
    global error_channel
    error_channel = client.get_channel(1021857059307802725) 
    print(error_channel.name)
    global archive_channel
    archive_channel = client.get_channel(1022206338027233280) 
    print(archive_channel.name)
    centro_banks_do.start()
    centro_banks_do_refrash.start()
    print("AG CB Start Success")
    print(f'Logged in as {client.user} (ID: {client.user.id})')
    print('------')

@tasks.loop(minutes=5)
async def centro_banks_do():
    if(centro_banks_do.current_loop==0):
        return
    await archive_channel.send(f"```Круг начислений пользователей _№ {centro_banks_do.current_loop}```")

@tasks.loop(time=time(21,tzinfo=timezone.utc))
async def centro_banks_do_refrash():
    await archive_channel.send(f"```Круг обновлений _№ {centro_banks_do.current_loop}```")

@client.tree.command(name="choose")
@app_commands.describe(
    choices='Укажите варианты выбора через ",".',
    flags='Укажите флаги',)
async def choose(interaction: discord.Interaction, choices: str,flags:str = ""):
    choices = choices.split(",")
    if "p" in flags:
        await interaction.response.send_message(random.choice(choices),ephemeral=False)
    else:
        await interaction.response.send_message(random.choice(choices),ephemeral=True)

@client.tree.command(name="balance")
@app_commands.describe(
    pool='Укажите пул (по умолчанию пул гильдии)',
    flags='Укажите флаги',)
@app_commands.choices(pool = choice_avrest_pool_list)
async def balance_user_slash(interaction: discord.Interaction, pool:str = "0", flags:str = ""):
    if (pool =="0"):
        cursor.execute("SELECT pid FROM guilds WHERE gid=%i AND alive=1" % (interaction.guild.id))
        pid = cursor.fetchone() 
        pid = str(pid[0])
    else:
        pid = pool
    cursor.execute("SELECT cc FROM super WHERE did=%i AND alive=1" % (interaction.user.id))
    cc = cursor.fetchone()
    data_d = json.loads(cc[0])
    embed = discord.Embed(color = 0x1a73e8, title = 'Your PD:')
    for i in data_d[pid].keys():
        embed.add_field(name=data_d[pid][str(i)][1], value=str(data_d[pid][str(i)][0])+" "+str(i), inline=False)
    embed.set_footer(text=f"Обслуживающий пул №:{pid}.")
    if "p" in flags:
        await interaction.response.send_message(embed=embed, ephemeral=False)
    elif "l" in flags:
        await interaction.user.send(embed=embed)
        await interaction.response.send_message("Проверьте личные сообщения.", ephemeral=True)
    else:
        await interaction.response.send_message(embed=embed, ephemeral=True)
    await archive_channel.send(f"{interaction.user.display_name}:{interaction.user.id}\nbalance_slash {pool} - {flags}\n{interaction.guild.name}:{interaction.guild.id}")

@client.tree.context_menu(name='Баланс')
async def balance_user_manue(interaction: discord.Interaction, member: discord.Member):
    cursor.execute("SELECT is_hide, hide_reason FROM super WHERE did=%i AND alive=1" % (member.id))
    is_hide = cursor.fetchone()
    pid=None
    if (is_hide[0]==0):
        cursor.execute("SELECT pid FROM guilds WHERE gid=%i AND alive=1" % (interaction.guild.id))
        pid = cursor.fetchone() 
        pid = str(pid[0])
        cursor.execute("SELECT cc FROM super WHERE did=%i AND alive=1" % (member.id))
        cc = cursor.fetchone()
        data_d = json.loads(cc[0])
        embed = discord.Embed(color = 0x1a73e8, title = 'Your PD:')
        for i in data_d[pid].keys():
            embed.add_field(name=data_d[pid][str(i)][1], value=str(data_d[pid][str(i)][0])+" "+str(i), inline=False)
        embed.set_footer(text=f"Обслуживающий пул №:{pid}.")
        await interaction.user.send(f"Запрос баланса пользователя - {member.display_name}",embed=embed)
        await interaction.response.send_message("Проверьте личные сообщения.", ephemeral=True)
    else:
        await interaction.user.send(f"Данные пользователя сокрыты \nПричина сокрытия:\n{is_hide[1]}")
        await interaction.response.send_message("Проверьте личные сообщения.", ephemeral=True)
    await archive_channel.send(f"Сокрытие - {is_hide[0]} - {member.display_name} - {member.id}\n{interaction.user.display_name}:{interaction.user.id}\nbalance_manue {pid}\n{interaction.guild.name}:{interaction.guild.id}")

@client.tree.context_menu(name='Репорт пользователя')
async def report_user(interaction: discord.Interaction, member: discord.Member):
    model = report_modal(title="Репорт пользователя")
    superior_temp[model.custom_id]=member
    await interaction.response.send_modal(model)

@client.tree.context_menu(name='Репорт сообщения')
async def report_message(interaction: discord.Interaction, message: discord.Message):
    model = report_modal(title="Репорт сообщения")
    superior_temp[model.custom_id]=message
    await interaction.response.send_modal(model)
class report_modal(discord.ui.Modal):
    answ = discord.ui.TextInput(label='Причина',placeholder="Расскажите все что накипело, но по теме.", style=discord.TextStyle.paragraph)

    async def on_submit(self, interaction: discord.Interaction):
        cid=self.custom_id
        if (type(superior_temp[cid])== discord.message.Message):
            message=superior_temp[cid]
            superior_temp.pop(cid)
            await interaction.response.send_message(f'Спасибо за ваш репорт, сообщение {message.author.mention} отправлено на модерацию.', ephemeral=True)   
            embed = discord.Embed(title='Репорт сообщения')
            if message.content:
                embed.description = message.content
            embed.set_author(name=message.author.display_name, icon_url=message.author.display_avatar.url)
            embed.timestamp = message.created_at
            embed.add_field(name="Указанная причина",value=self.answ)
            embed.add_field(name=f"Контекст", value=f'Автор репорта:{interaction.user.id} \nАвтор сообщения:{message.author.id} \nКанал нарушения:{message.channel.id}', inline=False)
            embed.set_footer(text=f"Сервер:{interaction.guild.id}.")
            await report_channel.send("Репорт \n"+f'Автор репорта:{interaction.user.id} \nАвтор сообщения:{message.author.id} \nКанал нарушения:{message.channel.id}\n'+"Сообщение\n---\n"+"**"+message.content+"**"+"\n---\n"+str(message.created_at)+f"\n---\n Указанная причина \n```{self.answ}```\nmsg:"+str(message.id),embed=embed)
            self.stop()
            return
        elif (type(superior_temp[cid])== discord.member.Member):
            member=superior_temp[cid]
            superior_temp.pop(cid)
            await interaction.response.send_message(
            f'Спасибо за ваш репорт, в отношении пользователя {member.name} будет проведена проверка.', ephemeral=True
            )
            embed = discord.Embed(title='Репорт пользователя')
            embed.set_author(name=member.display_name, icon_url=member.display_avatar.url)
            embed.add_field(name="Указанная причина",value=self.answ)
            embed.add_field(name=f"Контекст", value=f"Репорт пользователя:{member.id} \n"+f'Автор репорта:{interaction.user.id} \nСервер нарушения:{interaction.guild.id}', inline=False)
            embed.set_footer(text=f"Сервер:{interaction.guild.id}.")
            await report_channel.send(f"Репорт пользователя:{member.id} \n"+f'Автор репорта:{interaction.user.id} \nСервер нарушения:{interaction.guild.id} \n Указанная причина\n```{self.answ}```',embed=embed)
            self.stop()
        else:
            await interaction.response.send_message("Что-то пошло не так.")
            self.stop()

@client.tree.command(name="give")
@app_commands.checks.cooldown(1,60,key=lambda i: (i.user.id))
@app_commands.describe(
    member='Укажите пользователя',
    currency='Укажите валюту',
    amount='Укажите количество',
    flags='Укажите флаги',)
@app_commands.choices( currency=choice_avrest_currences_list)
async def give_user_slash(interaction: discord.Interaction, member:discord.Member, currency:str,amount:app_commands.Range[int, 100, None],flags:str = ""):
    if(amount<100):
        await interaction.response.send_message("Переводы производятся от 100 ед любой валюты",ephemeral=True)
        return
    cursor.execute("SELECT pid,tax FROM guilds WHERE gid=%i AND alive=1" % (interaction.guild.id))
    pid = cursor.fetchone() 
    tax=int(pid[1])
    pid = str(pid[0])
    cursor.execute("SELECT cc FROM super WHERE did=%i AND alive=1" % (interaction.user.id))
    cc = cursor.fetchone()
    data_d = json.loads(cc[0])
    if(data_d[pid][currency][0]<amount):
        await interaction.response.send_message("Недостаточно валюты",ephemeral=True)
        return
    data_d[pid][currency][0]=data_d[pid][currency][0]-amount
    cursor.execute("UPDATE super SET cc = '%s' WHERE did = %i AND alive=1" % (str(json.dumps(data_d)),interaction.user.id))
    cursor.execute("SELECT tax FROM pools WHERE id=%i AND alive=1" % (int(pid)))
    ptax = cursor.fetchone() 
    ptax=int(ptax[0])
    guild_g_tax = int(-1 * (tax/100*amount) // 1 * -1)
    pool_g_tax = int(-1 * (ptax/100*amount) // 1 * -1)
    rmount=amount-guild_g_tax-pool_g_tax
    cursor.execute("SELECT cc FROM super WHERE did=%i AND alive=1" % (member.id))
    cc = cursor.fetchone()
    data_d = json.loads(cc[0])
    try:
        data_d[pid][currency][0]=data_d[pid][currency][0]+rmount
    except:
        cursor.execute("SELECT name FROM cent_pool WHERE short = '%s' " % (str(currency)))
        t = cursor.fetchone() 
        t=t[0]
        temp = {}
        temp[currency]=[rmount,t]
        data_d[pid]=temp
    cursor.execute("UPDATE super SET cc = '%s' WHERE did = %i AND alive=1" % (str(json.dumps(data_d)),member.id))
    await interaction.response.send_message(f"```Исходящий перевод\n{interaction.user.name} -> {member.name}\n{amount} {currency} ({guild_g_tax+pool_g_tax} tax)```", ephemeral=True)
    await member.send(f"```Входящий перевод\n{interaction.user.name} -> {member.name}\n{amount} {currency} ({guild_g_tax+pool_g_tax} tax)```")
    await archive_channel.send(f"```Перевод\n{interaction.user.name} -> {member.name}\n{amount} {currency} ({guild_g_tax+pool_g_tax} tax)```")
    try:
        superior_pool[pid][currency]=superior_pool[pid][currency]+pool_g_tax
    except:
        temp = {}
        temp[currency]=pool_g_tax
        superior_pool[pid]=temp
    try:
        superior_pool[interaction.guild.id]=superior_pool[interaction.guild.id]+guild_g_tax
    except:
        temp = {}
        temp[currency]=guild_g_tax
        superior_pool[interaction.guild.id]=temp

@client.tree.command(name="xpm__create_charge")
@app_commands.checks.cooldown(1,30,key=lambda i: (i.user.id))
@app_commands.describe(
    member='Укажите пользователя',
    pool='Укажите пул',
    currency='Укажите валюту',
    amount='Укажите количество',
    long='Укажите количество дней',
    flags='Укажите флаги',)
@app_commands.choices(pool=choice_avrest_pool_list,currency=choice_avrest_currences_list)
async def pool_management_create_charge(interaction: discord.Interaction, member:discord.Member,pool:str, currency:str,amount:app_commands.Range[int, 10, 10000],long:app_commands.Range[int, 1, 31],flags:str = ""):
    cursor.execute("SELECT data, currency_bank FROM pools WHERE id=%s AND alive=1" % (pool))
    cc = cursor.fetchone()
    pool_data_json = json.loads(str(cc[0]))
    if (interaction.user.id in pool_data_json["admins"]):
        pool_cb_json = json.loads(str(cc[1]))
        if(currency in pool_cb_json.keys()):
            pass
        else:
            await interaction.response.send_message("Валюта не зарегистрирована в пуле.",ephemeral=True)
            return
        if(pool_cb_json[currency]<(amount*long)):
            await interaction.response.send_message("В пуле недостаточно валюты.",ephemeral=True)
            return
        pool_cb_json[currency]=pool_cb_json[currency]-(amount*long)
        cursor.execute("UPDATE pools SET currency_bank = '%s' WHERE id = %s AND alive=1" % (str(json.dumps(pool_cb_json)),pool))
        cursor.execute("INSERT INTO pool_tickets (pid, member, curr_short, add_amount, times) VALUES (%s,%i,'%s',%i,%i)" % (pool,member.id,currency,amount,long))
    else:
        await interaction.response.send_message("Вы не являетесь администратором выбранного пула.",ephemeral=True)
    await interaction.response.send_message("OK.",ephemeral=True)

@client.tree.command(name="xpm__create_ticket")
@app_commands.checks.cooldown(1,30,key=lambda i: (i.user.id))
@app_commands.describe(
    member='Укажите пользователя которому придет код.',
    pool='Укажите пул',
    currency='Укажите валюту',
    amount='Укажите количество',
    lenght='Укажтите длинну кода',
    code='Придумайте код',
    flags='Укажите флаги',)
@app_commands.choices(pool=choice_avrest_pool_list,currency=choice_avrest_currences_list)
async def pool_management_create_ticket(interaction: discord.Interaction, pool:str, currency:str,amount:app_commands.Range[int, 1000, None], member:discord.Member|None,lenght:app_commands.Range[int,8,196]=16,code:str="any",flags:str = ""):
    if(member==None):
        member = interaction.user
    cursor.execute("SELECT data, currency_bank FROM pools WHERE id=%s AND alive=1" % (pool))
    cc = cursor.fetchone()
    pool_data_json = json.loads(str(cc[0]))
    if (interaction.user.id in pool_data_json["admins"]):
        pool_cb_json = json.loads(str(cc[1]))
        if(currency in pool_cb_json.keys()):
            pass
        else:
            await interaction.response.send_message("Валюта не зарегистрирована в пуле.",ephemeral=True)
            return
        if(pool_cb_json[currency]<amount):
            await interaction.response.send_message("В пуле недостаточно валюты.",ephemeral=True)
            return
        pool_cb_json[currency]=pool_cb_json[currency]-amount
        cursor.execute("UPDATE pools SET currency_bank = '%s' WHERE id = %s AND alive=1" % (str(json.dumps(pool_cb_json)),pool))
        cursor.execute("SELECT code FROM pool_codes")
        cc = cursor.fetchall()
        while((code in cc) or (len(code)<8) or (len(code)>256) or (code=="any")):
            code=secrets.token_urlsafe(lenght)
        if(flags!="rp"):
            code = code+"_"+pool
        cursor.execute("INSERT INTO pool_codes (pid, curr_short, add_amount, code) VALUES (%s,'%s',%i,'%s')" % (pool,currency,amount,code))
        await member.send(f"**Ure catch a code**\n<||{code}||>\n by {interaction.user.id}")
    else:
        await interaction.response.send_message("Вы не являетесь администратором выбранного пула.",ephemeral=True)
    await interaction.response.send_message("OK.",ephemeral=True)

@client.tree.command(name="use_ticket")
@app_commands.checks.cooldown(1,180,key=lambda i: (i.user.id))
@app_commands.describe(
    pool='Укажите пул в который поступит валюта',
    code='Напишите код',
    flags='Укажите флаги',)
@app_commands.choices(pool=choice_avrest_pool_list)
async def pool_management_create_ticket(interaction: discord.Interaction, pool:str|None,code:str,flags:str = ""):
    if(pool==None):
        cursor.execute("SELECT pid FROM guilds WHERE gid=%i AND alive=1" % (interaction.guild.id))
        cc = cursor.fetchone()
        pool = cc[0]
    cursor.execute("SELECT * FROM pool_codes WHERE code='%s'"%(code))
    cc = cursor.fetchone()
    if(len(cc)==0):
        await interaction.response.send_message("Пока что не угадал.",ephemeral=True)
        return
    cursor.execute("SELECT cc FROM super WHERE did=%i AND alive=1" % (interaction.user.id))
    ccu = cursor.fetchone()
    user_json = json.loads(ccu[0])
    try:
        user_json[cc[1]][cc[2]][0]=int(user_json[cc[1]][cc[2]][0])+int(cc[3])
    except:
        cursor.execute("SELECT name FROM cent_pool WHERE short = '%s' " % (str(cc[2])))
        t = cursor.fetchone() 
        t=t[0]
        temp = {}
        temp[cc[2]]=[cc[3],t]
        user_json[cc[1]]=temp
    cursor.execute("DELETE FROM pool_codes WHERE id=%s"%(str(cc[0])))
    cursor.execute("UPDATE super SET cc = '%s' WHERE did = %i AND alive=1" % (str(json.dumps(user_json)),interaction.user.id))
    await interaction.response.send_message(f"Использован Цифровой код\nПолучено {cc[3]} {cc[2]}\nОбслуживающий пул №{cc[1]}",ephemeral=True)

@client.tree.command(name="role")
@app_commands.checks.cooldown(1,60,key=lambda i: (i.user.id))
async def role(interaction: discord.Interaction,what_to_do:Literal["info","up"]):
    cursor.execute("SELECT data,main_curr_short,pid FROM guilds WHERE gid=%i AND alive=1" % (interaction.guild.id))
    cc = cursor.fetchone()
    guild_roles = json.loads(cc[0])
    roles = guild_roles["roles"] 
    info = "```Roles info\n"
    hight_role=0
    for i in roles.keys(): #{lvl:{id:id,price:price}}
        temp = interaction.guild._roles[roles[i]["id"]]
        roles[i]["role"]= temp
        if(temp in interaction.user.roles):
            hight_role=int(i)
            try:
                info+=f"${i} {roles[i]['role'].name} {roles[i]['price']} {cc[1]}\n"
            except:
                info+=f"${i} Не возможно найти роль в гильдии  {roles[i]['price']}\n"
        else:
            try:
                info+=f"№{i} {roles[i]['role'].name} {roles[i]['price']} {cc[1]}\n"
            except:
                info+=f"№{i} **Не возможно найти роль в гильдии**  {roles[i]['price']}\n"
    info+="```"
    if(what_to_do=="info"):
        await interaction.response.send_message(info,ephemeral=True)
        return
    elif(what_to_do=="up"):
        if(len(roles)==(hight_role+1)):
            await interaction.response.send_message("Вы уже получили самую высокую роль.",ephemeral=True)
            return
        cursor.execute("SELECT cc FROM super WHERE did=%i AND alive=1" % (interaction.user.id))
        ccu = cursor.fetchone()
        data_d = json.loads(ccu[0])
        if(data_d[str(cc[2])][cc[1]][0]<roles[str(hight_role+1)]['price']):
            await interaction.response.send_message("Недостаточно валюты",ephemeral=True)
            return
        data_d[str(cc[2])][cc[1]][0]=data_d[str(cc[2])][cc[1]][0]-roles[str(hight_role+1)]['price']
        cursor.execute("UPDATE super SET cc = '%s' WHERE did = %i AND alive=1" % (str(json.dumps(data_d)),interaction.user.id))
        await interaction.user.add_roles(roles[str(hight_role+1)]['role'],reason="Avrest.role.up")
        await interaction.user.remove_roles(roles[str(hight_role)]['role'],reason="Avrest.role.up")
        cursor.execute("SELECT cur_amount FROM cent_pool WHERE short='%s'" % (cc[1]))
        ccu = cursor.fetchone()
        cursor.execute("UPDATE cent_pool SET cur_amount = %i WHERE short='%s'" % (ccu[0]+int(roles[str(hight_role+1)]['price']),cc[1]))
        await interaction.response.send_message("Роль повышена)",ephemeral=True)
        return
    await interaction.response.send_message("Как?",ephemeral=True)

@client.tree.command(name="ethalon")
@app_commands.checks.cooldown(1,5,key=lambda i: (i.user.id))
async def ethalon(interaction: discord.Interaction,what_wiki:Literal["wiki_ru1","wiki_ru2","wiki_en"],what_to_know:str):
    synonyms = ["Ищем...","Поиск...","Сейчас скажу","Сейчас найду","Дой падуматб","...","( ˙꒳​˙ )","(´･ᴗ･ )","Колдую...","Вспоминаю"]
    otvet = ["Готово","Выдача","Нашел","Вспомнил","Как-то так","KK","Наколдовал"]
    await interaction.response.send_message(random.choice(synonyms),ephemeral=True)
    if(what_wiki=="wiki_ru1"):
        res = client_wolf.query(what_to_know)
        embed=discord.Embed(title=str(what_to_know), url='', description=' ', color=0xFF5733)
        for pod in res.pods:
            embed.add_field(name=str(pod.title), value=str(pod.text), inline=False)
        await interaction.edit_original_response(content=random.choice(otvet),embed=embed)
    elif(what_wiki=="wiki_ru2"):
        wikipedia.set_lang("ru")
        ny = wikipedia.page(what_to_know)
        embed=discord.Embed(title=ny.title, url=ny.url, description=wikipedia.summary(str(what_to_know), sentences=5), color=0xFF5733)
        await interaction.edit_original_response(content=random.choice(otvet),embed=embed)
    elif(what_wiki=="wiki_en"):
        wikipedia.set_lang("en")
        ny = wikipedia.page(what_to_know)
        embed=discord.Embed(title=ny.title, url=ny.url, description=wikipedia.summary(str(what_to_know), sentences=5), color=0xFF5733)
        await interaction.edit_original_response(content=random.choice(otvet),embed=embed)
    else:
        await interaction.edit_original_response("Как?")

@client.tree.command(name="lottery")
@app_commands.checks.cooldown(1,10,key=lambda i: (i.user.id))
@app_commands.describe(
    currency='Укажите валюту',
    amount='Укажите количество',
    flags='Укажите флаги',)
@app_commands.choices(currency=choice_avrest_currences_list)
async def lottery(interaction: discord.Interaction,currency:str,amount:app_commands.Range[int, 100, None],your_1_number:app_commands.Range[int,0,255],your_2_number:app_commands.Range[int,0,255],your_3_number:app_commands.Range[int,0,255],your_4_number:app_commands.Range[int,0,255],your_5_number:app_commands.Range[int,0,255],flags:str = ""):
    await interaction.response.send_message("Проверка...",ephemeral=True)
    cursor.execute("SELECT pid FROM guilds WHERE gid=%i AND alive=1" % (interaction.guild.id))
    cc = cursor.fetchone()
    pool = str(cc[0])
    cursor.execute("SELECT cc FROM super WHERE did=%i AND alive=1" % (interaction.user.id))
    ccu = cursor.fetchone()
    data_d = json.loads(ccu[0])
    cursor.execute("SELECT cur_amount FROM cent_pool WHERE short='%s'" % (currency))
    ccu = cursor.fetchone()
    ccu = int(ccu[0])+amount
    if(data_d[pool][currency][0]<amount):
        await interaction.edit_original_response(content="Недостаточно валюты")
        return
    data_d[pool][currency][0]=data_d[pool][currency][0]-amount
    lott_amount = amount-int(-1 * (5/100*amount) // 1 * -1)
    lucky_numbers = []
    user_numbers = [your_1_number,your_2_number,your_3_number,your_4_number,your_5_number]
    for num in range(0,5):
        random_num = random.randint(0, 255)
        lucky_numbers.append(random_num)
    result=list(set(user_numbers) & set(lucky_numbers))
    correct_numbers=len(result)
    if (correct_numbers>=1):
        lott_amount=(5**correct_numbers)*lott_amount
        ccu-=lott_amount
        data_d[pool][currency][0]=data_d[pool][currency][0]+lott_amount
        await interaction.edit_original_response(content=f"```Вы поставили {amount} {currency}\nВы угадали {correct_numbers} чисел\nВы выйграли {lott_amount} {currency}\nПрибыль {lott_amount-amount} {currency}\n{user_numbers}\n{lucky_numbers}```")
    else:
        await interaction.edit_original_response(content=f"```Вы поставили {amount} {currency}\nВы угадали {correct_numbers} чисел\nВы проиграли\n{user_numbers}\n{lucky_numbers}```")
    cursor.execute("UPDATE cent_pool SET cur_amount = %i WHERE short='%s'" % (ccu,currency))
    cursor.execute("UPDATE super SET cc = '%s' WHERE did = %i AND alive=1" % (str(json.dumps(data_d)),interaction.user.id))







@client.tree.command(name="info")
@app_commands.checks.cooldown(1,300,key=lambda i: (i.user.id))
@app_commands.describe()
async def choose(interaction: discord.Interaction):
    await interaction.user.send("Ok.")

@client.tree.command(name="x_")
@commands.has_guild_permissions(administrator=True)
@app_commands.describe(
    choices='Укажите варианты выбора через ",".',
    flags='Укажите флаги',
)
async def choose(interaction: discord.Interaction, choices: str,flags:str = ""):
    raise 1


client.run(avrest_const['token']) # Обращаемся к словарю settings с ключом token, для получения токена
