from time import time
import mysql.connector
import random
import logging
from aiogram import Bot, Dispatcher, executor, types
import secrets
import string
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.dispatcher import FSMContext
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.types import ReplyKeyboardRemove, \
    ReplyKeyboardMarkup, KeyboardButton, \
    InlineKeyboardMarkup, InlineKeyboardButton

import mariadb
import psycopg2

API_TOKEN = ''

hi_patterns = ["Кажется я тебя уже знаю...","Откуда же я тебя знаю?","Кто-то знакомый, но кто ты?","Настойчивость это хорошо.","Определяться и правда сложно.","НИНАДА ПЖЛСТ","!?"]

emotions = []


# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot,storage=MemoryStorage())


cnx = mysql.connector.connect(user='', password='',host='',port='',database='')
cnx.autocommit = True
cursor = cnx.cursor(buffered=True)

cnx_a = mysql.connector.connect(user='', password='',host='',port='',database='')
cnx_a.autocommit = True
cursor_a = cnx_a.cursor(buffered=True)


class sql_connection:
    def __init__(self, type, server):
        server = server+".avrest.ru"

        if(type=="mysql" or type=="mysqlx"):
            self.cnx_v = mysql.connector.connect(user='', password='',host=server,port='',database='')
            self.cnx_v.autocommit = True
            self.cursor = self.cnx_v.cursor(buffered=True)
        elif(type=="postgre"):
            self.cnx_v = psycopg2.connect(host=server, database="", user="", password="")
            self.cnx_v.autocommit = True
            self.cursor = self.cnx_v.cursor()
        elif(type=="postgrex"):
            self.cnx_v = psycopg2.connect(host=server, database="", user="", password="")
            self.cnx_v.autocommit = True
            self.cursor = self.cnx_v.cursor()
        elif(type=="mariadb" or type=="mariadbx"):
            self.cnx_v = mariadb.connect(user="",password="",host=server,port="",database="")
            self.cnx_v.autocommit = True
            self.cursor = self.cnx_v.cursor(buffered=True)
        else:
            self.cursor = None


##########################################################################################################


button_db_core = KeyboardButton(text="Avrest Data")
button_disc_core = KeyboardButton(text="Avrest on Discord")
button_click_core = KeyboardButton(text="Avrest Click")

greet_kb = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
greet_kb.add(button_db_core)
greet_kb.add(button_disc_core)
greet_kb.add(button_click_core)

##########################################################################################################

def message_splitter(message):
    message = message.split(" ")
    message.pop(0)
    return message




@dp.message_handler(commands=['start'])
async def send_welcome(message: types.Message):
    cursor.execute("SELECT id FROM users_data WHERE t_id=%i" % (message.chat.id))
    u_id = cursor.fetchone()  
    if u_id is not None:
        await message.reply("(?.?)"+random.choice(hi_patterns),reply_markup=greet_kb)
    else:
        cursor.execute("INSERT INTO users_data (t_id) VALUES (%i)" % (message.chat.id))
        await message.reply("\(0.0)/",reply_markup=greet_kb)

@dp.message_handler(commands=['gotest'])
async def send_gotest(message: types.Message):
    await message.reply("Заявка принята.")
        
@dp.message_handler(commands=['noty'])
async def send_noty(message: types.Message):
    cursor.execute("SELECT is_noty FROM users_data WHERE t_id=%i" % (message.chat.id))
    u_id = cursor.fetchone()
    u_id=u_id[0]
    if (u_id==1):
        cursor.execute("UPDATE users_data SET is_noty = 0 WHERE t_id = %i " % (message.chat.id))
        await message.answer("Базовые уведомления отключены.")
    elif(u_id==0):
        cursor.execute("UPDATE users_data SET is_noty = 1 WHERE t_id = %i " % (message.chat.id))
        await message.answer("Базовые уведомления подключены.")
    else:
        cursor.execute("UPDATE users_data SET is_noty = 1 WHERE t_id = %i " % (message.chat.id))
        await message.answer("Базовые уведомления подключены.")

#########
#DISCORD#
#########
@dp.message_handler(commands=['connect'])
async def avrest_connect(message: types.Message):
    message_content = message_splitter(message.text)
    if len(message_content)==0:
        return
    cursor_a.execute("SELECT id FROM guilds_data WHERE g_privat_key=%s" % (message_content[0]))
    u_id = cursor_a.fetchone()
    if len(u_id)==0:
        return
    id_to_connect = u_id[0]
    cursor.execute("SELECT connection FROM users_connections WHERE user=%i" % (message.chat.id))
    connected_ids = cursor.fetchall()
    for i in range(len(connected_ids)):
        connected_ids[i] = connected_ids[i][0]
    if id_to_connect in connected_ids:
        await message.answer(f"Гильдия {id_to_connect} уже подключена.")
        return
    cursor.execute("INSERT INTO users_connections (user, connection) VALUES (%i,%i)" % (message.chat.id,id_to_connect))
    cnx.commit()
    await message.answer(f"Подключение гильдии {id_to_connect} успешно.")

@dp.message_handler(commands=['guilds'])
async def avrest_guilds(message: types.Message):
    cursor.execute("SELECT connection FROM users_connections WHERE user=%i" % (message.chat.id))
    connected_ids = cursor.fetchall()
    if len(connected_ids)==0:
        return
    for i in range(len(connected_ids)):
        connected_ids[i] = connected_ids[i][0]
    keyboard_markup = types.InlineKeyboardMarkup(row_width=3, one_time_keyboard=True)
    for i in connected_ids:
        keyboard_markup.add(types.InlineKeyboardButton(str(i), callback_data="disc/s/guild/s/"+str(i)))
    await message.answer("Подключенные гильдии:", reply_markup=keyboard_markup)


######
#MYSQL
######

class create_user_sql(StatesGroup):
    db_core = State()
    location = State()
    user_name = State()
    confirm = State()

@dp.message_handler(state=create_user_sql.db_core)
async def process_message(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['db'] = message.text.lower()
        db = data['db']
    markup = types.ReplyKeyboardRemove()
    if(db=="Отмена"):
        await bot.send_message(message.from_user.id, f"Создание пользователя прервано.",reply_markup=markup)
        await state.finish()
        return
    try:
        cursor.execute(f"SELECT db_location FROM db_locations WHERE db_type = '"+str(db)+"'")
        slots = cursor.fetchall()
    except:
        await bot.send_message(message.from_user.id, f"Выбор небыл распознан, попробуйте начать сначала.",reply_markup=markup)
        await state.finish()
        return
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
    for slot in slots:
        markup.add(slot[0])
    markup.add("Отмена")
    await create_user_sql.location.set()
    await bot.send_message(message.from_user.id, f"Выбирите желаемое местоположение:", reply_markup=markup) 

@dp.message_handler(state=create_user_sql.location)
async def process_message(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['loc'] = message.text
        db_location = data['loc']
    markup = types.ReplyKeyboardRemove()
    if(db_location=="Отмена"):
        await bot.send_message(message.from_user.id, f"Создание пользователя прервано.",reply_markup=markup)
        await state.finish()
        return
    await bot.send_message(message.from_user.id, f"Вы выбрали расположение {db_location}",reply_markup=markup)
    await bot.send_message(message.from_user.id, f"Укажите имя пользователя:")
    await create_user_sql.user_name.set()       

@dp.message_handler(state=create_user_sql.user_name)
async def process_message(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['name'] = message.text
        user_message = data['name']
        db_location = data['loc']
    name = "".join(c for c in user_message if c.isalnum())
    if((name=="No") or (name=="no")):
        await bot.send_message(message.from_user.id, f"Создание пользователя прервано.")
        await state.finish()
        return
    cursor.execute("SELECT user FROM db_users")
    already_exists = cursor.fetchall()
    already_exists_2 = []
    if((len(name)<4) or (len(name)>10)):
        await bot.send_message(message.from_user.id, f"Выбранное имя: {name} должно быть от 4х до 10 символов.\nВведите новое или 'No' чтобы прекратить создание пользователя.")
        return
    for i in already_exists:
        already_exists_2.append(i[0])
    if name in already_exists_2:
        await bot.send_message(message.from_user.id, f"Выбранное имя: {name} занято или не может существовать.\nВведите новое или 'No' чтобы прекратить создание пользователя.")
        return 
    await bot.send_message(message.from_user.id, f"Вы выбрали имя: {name}, подтвердить?")
    await create_user_sql.confirm.set()
    
@dp.message_handler(state=create_user_sql.confirm)
async def process_message(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['conf'] = message.text
        conf = data['conf']
        name = data['name']
        loc = data['loc']
        db = data['db']
    if ((conf=="yes")or(conf=="Yes")or(conf=="Y")or(conf=="y")or(conf=="Да")or(conf=="да")or(conf=="Подтвердить")or(conf=="подтвердить")or(conf=="Confirm")or(conf=="confirm")or(conf=="da")or(conf=="Da")):
        cursor.execute("SELECT user FROM db_users")
        already_exists = cursor.fetchall()
        already_exists_2 = []
        for i in already_exists:
            already_exists_2.append(i[0])
        while True:
            secret_post = secrets.SystemRandom().randint(1000, 9999)
            temp = name+"_"+str(secret_post)
            if temp in already_exists_2:
                continue
            else:
                break
        alphabet = string.ascii_letters + string.digits
        while True:
            secret_pass = ''.join(secrets.choice(alphabet) for i in range(24))
            if (any(c.islower() for c in secret_pass)
                    and any(c.isupper() for c in secret_pass)
                    and sum(c.isdigit() for c in secret_pass) >= 3):
                break

        if (db=="mysql"):
            timed= sql_connection(db, loc)
            timed_curr = timed.cursor
            timed_curr.execute("CREATE USER '"+str(temp)+"'@'%' IDENTIFIED BY '"+str(secret_pass)+"' WITH MAX_QUERIES_PER_HOUR 360 MAX_CONNECTIONS_PER_HOUR 6 MAX_USER_CONNECTIONS 1;")
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            timed = sql_connection("mysql", "ru1")
            timed_curr = timed.cursor
            timed_curr.execute("INSERT INTO db_users (tg, user, secret, db_type,db_location) VALUES ('%i', '%s', '%s', '%s', '%s');" % (message.chat.id,temp,secret_pass,db,loc))
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            port = 3306

        elif (db=="postgre"):
            timed = sql_connection(db, loc)
            timed_curr = timed.cursor
            timed_curr.execute("CREATE USER "+str(temp)+" WITH PASSWORD '"+str(secret_pass)+"' CONNECTION LIMIT 1;")
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            timed = sql_connection("mysql", "ru1")
            timed_curr = timed.cursor
            timed_curr.execute("INSERT INTO db_users (tg, user, secret, db_type,db_location) VALUES ('%i', '%s', '%s', '%s', '%s');" % (message.chat.id,temp,secret_pass,db,loc))
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            port = 5432

        elif (db=="mariadb"):
            timed = sql_connection(db, loc)
            timed_curr = timed.cursor
            timed_curr.execute("CREATE USER '"+str(temp)+"'@'%' IDENTIFIED BY '"+str(secret_pass)+"' WITH MAX_USER_CONNECTIONS 1;")
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            timed = sql_connection("mysql", "ru1")
            timed_curr = timed.cursor
            timed_curr.execute("INSERT INTO db_users (tg, user, secret, db_type,db_location) VALUES ('%i', '%s', '%s', '%s', '%s');" % (message.chat.id,temp,secret_pass,db,loc))
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            port = 3306

        await bot.send_message(message.from_user.id, 
                            "Создание пользователя успешно.\n"
                            f"adress: {loc}.avrest.ru\n"
                            f"port: {port}\n"
                            f"login: {temp}\n"
                            f"password: {secret_pass}")
        await state.finish()
    else:
        await bot.send_message(message.from_user.id, f"Введите новое или 'No' чтобы прекратить создание пользователя.")
        await create_user_sql.user_name.set()
        
        


       
        
class create_schema_sql(StatesGroup):
    db_core = State()
    location = State()
    schema_name = State()
    confirm = State()

@dp.message_handler(state=create_schema_sql.db_core)
async def process_message(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['db'] = message.text.lower()
        db = data['db']
    markup = types.ReplyKeyboardRemove()
    if(db=="Отмена"):
        await bot.send_message(message.from_user.id, f"Создание схемы прервано.",reply_markup=markup)
        await state.finish()
        return
    try:
        cursor.execute(f"SELECT db_location FROM db_locations WHERE db_type = '"+str(db)+"'")
        slots = cursor.fetchall()
    except:
        await bot.send_message(message.from_user.id, f"Выбор небыл распознан, попробуйте начать сначала.",reply_markup=markup)
        await state.finish()
        return
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
    for slot in slots:
        markup.add(slot[0])
    markup.add("Отмена")
    await create_schema_sql.location.set()
    await bot.send_message(message.from_user.id, f"Выбирите желаемое местоположение:", reply_markup=markup) 

@dp.message_handler(state=create_schema_sql.location)
async def process_message(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['loc'] = message.text
        db_location = data['loc']
    markup = types.ReplyKeyboardRemove()
    if(db_location=="Отмена"):
        await bot.send_message(message.from_user.id, f"Создание схемы прервано.",reply_markup=markup)
        await state.finish()
        return
    await bot.send_message(message.from_user.id, f"Вы выбрали расположение {db_location}",reply_markup=markup)
    await bot.send_message(message.from_user.id, f"Укажите название схемы:")
    await create_schema_sql.schema_name.set()       

@dp.message_handler(state=create_schema_sql.schema_name)
async def process_message(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['name'] = message.text
        user_message = data['name']
        db_location = data['loc']
    name = "".join(c for c in user_message if c.isalnum())
    if((name=="No") or (name=="no")):
        await bot.send_message(message.from_user.id, f"Создание схемы прервано.")
        await state.finish()
        return
    cursor.execute("SELECT schema_name FROM db_schemas")
    already_exists = cursor.fetchall()
    already_exists_2 = []
    if((len(name)<4) or (len(name)>10)):
        await bot.send_message(message.from_user.id, f"Выбранное название: {name} должно быть от 4х до 20 символов.\nВведите новое или 'No' чтобы прекратить создание схемы.")
        return
    for i in already_exists:
        already_exists_2.append(i[0])
    if name in already_exists_2:
        await bot.send_message(message.from_user.id, f"Выбранное название: {name} занято или не может существовать.\nВведите новое или 'No' чтобы прекратить создание схемы.")
        return 
    await bot.send_message(message.from_user.id, f"Вы выбрали название: {name}, подтвердить?")
    await create_schema_sql.confirm.set()
    
@dp.message_handler(state=create_schema_sql.confirm)
async def process_message(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['conf'] = message.text
        conf = data['conf']
        name = data['name']
        loc = data['loc']
        db = data['db']
    if ((conf=="yes")or(conf=="Yes")or(conf=="Y")or(conf=="y")or(conf=="Да")or(conf=="да")or(conf=="Подтвердить")or(conf=="подтвердить")or(conf=="Confirm")or(conf=="confirm")or(conf=="da")or(conf=="Da")):
        cursor.execute("SELECT schema_name FROM db_schemas")
        already_exists = cursor.fetchall()
        already_exists_2 = []
        for i in already_exists:
            already_exists_2.append(i[0])
        while True:
            secret_post = secrets.SystemRandom().randint(1000, 9999)
            temp = name+"_"+str(secret_post)
            if temp in already_exists_2:
                continue
            else:
                break
        alphabet = string.ascii_letters + string.digits
        while True:
            secret_pass = ''.join(secrets.choice(alphabet) for i in range(24))
            if (any(c.islower() for c in secret_pass)
                    and any(c.isupper() for c in secret_pass)
                    and sum(c.isdigit() for c in secret_pass) >= 3):
                break

        if (db=="mysql"):
            timed = sql_connection(db, loc)
            timed_curr = timed.cursor
            timed_curr.execute("CREATE DATABASE "+str(temp)+";")
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            timed = sql_connection("mysql", "ru1")
            timed_curr = timed.cursor
            timed_curr.execute("INSERT INTO db_schemas (tg, schema_name, db_type,db_location) VALUES ('%i', '%s', '%s', '%s');" % (message.chat.id,temp,db,loc))
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            port = 3306

        elif (db=="postgre"):
            timed = sql_connection(db, loc)
            timed_curr = timed.cursor
            timed_curr.execute("CREATE SCHEMA "+str(temp)+";")
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            timed = sql_connection("mysql", "ru1")
            timed_curr = timed.cursor
            timed_curr.execute("INSERT INTO db_schemas (tg, schema_name, db_type,db_location) VALUES ('%i', '%s', '%s', '%s');" % (message.chat.id,temp,db,loc))
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            port = 5432

        elif (db=="mariadb"):
            timed = sql_connection(db, loc)
            timed_curr = timed.cursor
            timed_curr.execute("CREATE DATABASE "+str(temp)+";")
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            timed = sql_connection("mysql", "ru1")
            timed_curr = timed.cursor
            timed_curr.execute("INSERT INTO db_schemas (tg, schema_name, db_type,db_location) VALUES ('%i', '%s', '%s', '%s');" % (message.chat.id,temp,db,loc))
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            port = 3306

        await bot.send_message(message.from_user.id, 
                            "Создание схемы успешно.\n"
                            f"adress: {loc}.avrest.ru\n"
                            f"type: {db}\n"
                            f"name: {temp}\n")
        await state.finish()
    else:
        await bot.send_message(message.from_user.id, f"Введите новое или 'No' чтобы прекратить создание схемы.")
        await create_schema_sql.schema_name.set()        







class create_table_sql(StatesGroup):
    db_core = State()
    location = State()
    table_name = State()
    confirm = State()

@dp.message_handler(state=create_table_sql.db_core)
async def process_message(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['db'] = message.text.lower()
        db = data['db']
    markup = types.ReplyKeyboardRemove()
    if(db=="Отмена"):
        await bot.send_message(message.from_user.id, f"Создание таблицы прервано.",reply_markup=markup)
        await state.finish()
        return
    try:
        cursor.execute(f"SELECT db_location FROM db_locations WHERE db_type = '"+str(db)+"'")
        slots = cursor.fetchall()
    except:
        await bot.send_message(message.from_user.id, f"Выбор небыл распознан, попробуйте начать сначала.",reply_markup=markup)
        await state.finish()
        return
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
    for slot in slots:
        markup.add(slot[0])
    markup.add("Отмена")
    await create_table_sql.location.set()
    await bot.send_message(message.from_user.id, f"Выбирите желаемое местоположение:", reply_markup=markup) 

@dp.message_handler(state=create_table_sql.location)
async def process_message(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['loc'] = message.text
        db_location = data['loc']
    markup = types.ReplyKeyboardRemove()
    if(db_location=="Отмена"):
        await bot.send_message(message.from_user.id, f"Создание таблицы прервано.",reply_markup=markup)
        await state.finish()
        return
    await bot.send_message(message.from_user.id, f"Вы выбрали расположение {db_location}",reply_markup=markup)
    await bot.send_message(message.from_user.id, f"Укажите название таблицы:")
    await create_table_sql.table_name.set()       

@dp.message_handler(state=create_table_sql.table_name)
async def process_message(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['name'] = message.text
        user_message = data['name']
        db_location = data['loc']
    name = "".join(c for c in user_message if c.isalnum())
    if((name=="No") or (name=="no")):
        await bot.send_message(message.from_user.id, f"Создание таблицы прервано.")
        await state.finish()
        return
    cursor.execute("SELECT db FROM db_db")
    already_exists = cursor.fetchall()
    already_exists_2 = []
    if((len(name)<4) or (len(name)>10)):
        await bot.send_message(message.from_user.id, f"Выбранное название: {name} должно быть от 4х до 20 символов.\nВведите новое или 'No' чтобы прекратить создание таблицы.")
        return
    for i in already_exists:
        already_exists_2.append(i[0])
    if name in already_exists_2:
        await bot.send_message(message.from_user.id, f"Выбранное название: {name} занято или не может существовать.\nВведите новое или 'No' чтобы прекратить создание таблицы.")
        return 
    await bot.send_message(message.from_user.id, f"Вы выбрали название: {name}, подтвердить?")
    await create_table_sql.confirm.set()
    
@dp.message_handler(state=create_table_sql.confirm)
async def process_message(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['conf'] = message.text
        conf = data['conf']
        name = data['name']
        loc = data['loc']
        db = data['db']
    if ((conf=="yes")or(conf=="Yes")or(conf=="Y")or(conf=="y")or(conf=="Да")or(conf=="да")or(conf=="Подтвердить")or(conf=="подтвердить")or(conf=="Confirm")or(conf=="confirm")or(conf=="da")or(conf=="Da")):
        cursor.execute("SELECT db FROM db_db")
        already_exists = cursor.fetchall()
        already_exists_2 = []
        for i in already_exists:
            already_exists_2.append(i[0])
        while True:
            secret_post = secrets.SystemRandom().randint(1000, 9999)
            temp = name+"_"+str(secret_post)
            if temp in already_exists_2:
                continue
            else:
                break
        alphabet = string.ascii_letters + string.digits
        while True:
            secret_pass = ''.join(secrets.choice(alphabet) for i in range(24))
            if (any(c.islower() for c in secret_pass)
                    and any(c.isupper() for c in secret_pass)
                    and sum(c.isdigit() for c in secret_pass) >= 3):
                break

        if (db=="mysql"):
            timed = sql_connection(db, loc)
            timed_curr = timed.cursor
            timed_curr.execute("CREATE TABLE isolate."+str(temp)+" (id int);;")
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            timed = sql_connection("mysql", "ru1")
            timed_curr = timed.cursor
            timed_curr.execute("INSERT INTO db_db (tg, db, db_type,db_location) VALUES ('%i', '%s', '%s', '%s');" % (message.chat.id,temp,db,loc))
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            port = 3306

        elif (db=="postgre"):
            timed = sql_connection(db+"x", loc)
            timed_curr = timed.cursor
            timed_curr.execute("CREATE TABLE isolate."+str(temp)+" (id int);")
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            timed = sql_connection("mysql", "ru1")
            timed_curr = timed.cursor
            timed_curr.execute("INSERT INTO db_db (tg, db, db_type,db_location) VALUES ('%i', '%s', '%s', '%s');" % (message.chat.id,temp,db,loc))
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            port = 5432

        elif (db=="mariadb"):
            timed = sql_connection(db, loc)
            timed_curr = timed.cursor
            timed_curr.execute("CREATE TABLE isolate."+str(temp)+" (id int);")
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            timed = sql_connection("mysql", "ru1")
            timed_curr = timed.cursor
            timed_curr.execute("INSERT INTO db_db (tg, db, db_type,db_location) VALUES ('%i', '%s', '%s', '%s');" % (message.chat.id,temp,db,loc))
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            port = 3306

        await bot.send_message(message.from_user.id, 
                            "Создание таблицы успешно.\n"
                            f"adress: {loc}.avrest.ru\n"
                            f"core: {db}\n"
                            f"schema//database: isolate\n"
                            f"name: {temp}\n")
        await state.finish()
    else:
        await bot.send_message(message.from_user.id, f"Введите новое или 'No' чтобы прекратить создание таблицы.")
        await create_table_sql.table_name.set()      
      
######
######
######
@dp.callback_query_handler()
async def inline_kb_answer_callback_handler(query: types.CallbackQuery):
    try:
        data = query.data.split("/s/")
        while (len(data)<10):
            data.append("None")
    except:
        await bot.send_message(query.from_user.id, "Ошибка...")
        return
    
    
    
    
    
    if(data[0]=="disc"):
        
        if(data[1]=="guild"):
            cursor.execute("UPDATE users_data SET current_operation_disc = %s WHERE t_id = %i;" % (data,query.from_user.id))
            await bot.send_message(query.from_user.id, f"Выбрана гильдия {data}")
    






    elif(data[0]=="db"):
        if(data[1]=="users"):
            if (data[2]=="users"):
                cursor.execute("SELECT user, db_type, db_location FROM db_users WHERE tg=%i" % (query.from_user.id))
                connected_ids = cursor.fetchall()
                cursor.execute("SELECT max_db_users FROM users_data WHERE t_id=%i" % (query.from_user.id))
                slots = cursor.fetchone()
                keyboard_markup = types.InlineKeyboardMarkup(row_width=3, one_time_keyboard=True)
                if len(connected_ids)==0:
                    keyboard_markup.add(types.InlineKeyboardButton(f"Создать пользователя[{slots[0]}]", callback_data="db/s/users/s/create/s/create"))
                    await bot.send_message(query.from_user.id, f"У вас нет ни одного пользователя, хотите создать?", reply_markup=keyboard_markup)
                    return
                for i in range(len(connected_ids)):
                    connected_ids[i] = [connected_ids[i][0],connected_ids[i][1],connected_ids[i][2]]
                for i in connected_ids:
                    keyboard_markup.add(types.InlineKeyboardButton(str(i[1])+":"+str(i[0])+":"+str(i[2]), callback_data=f"db/s/users/s/{i[1]}/s/see/s/{i[0]}"))
                if (slots[0]-len(connected_ids)>0):
                    keyboard_markup.add(types.InlineKeyboardButton(f"Создать пользователя[{slots[0]-len(connected_ids)}]", callback_data="db/s/users/s/create/s/create"))
                await bot.send_message(query.from_user.id, f"Ваши пользователи:", reply_markup=keyboard_markup)    
                    
            
            
            
            elif(data[2]=="create"):
                markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
                markup.add("MySQL")
                markup.add("PostGre")
                markup.add("MariaDB")
                markup.add("Отмена")
                await create_user_sql.db_core.set()
                await bot.send_message(query.from_user.id, f"Доступные движки:", reply_markup=markup)  
            
            
            
            elif(data[2]=="mysql" or data[2]=="postgre" or data[2]=="mariadb"):
                if(data[3]=="see"):
                    keyboard_markup = types.InlineKeyboardMarkup(row_width=3, one_time_keyboard=True)
                    keyboard_markup.add(types.InlineKeyboardButton("Info", callback_data=f"db/s/users/s/{data[2]}/s/info/s/{data[4]}"))
                    keyboard_markup.add(types.InlineKeyboardButton("Reset password", callback_data=f"db/s/users/s/{data[2]}/s/passreset/s/{data[4]}"))
                    keyboard_markup.add(types.InlineKeyboardButton("Grant priviliges", callback_data=f"db/s/users/s/{data[2]}/s/grant/s/{data[4]}/s/see"))
                    keyboard_markup.add(types.InlineKeyboardButton("Revoke priviliges", callback_data=f"db/s/users/s/{data[2]}/s/revoke/s/{data[4]}"))
                    keyboard_markup.add(types.InlineKeyboardButton("Drop", callback_data=f"db/s/users/s/{data[2]}/s/drop/s/{data[4]}"))
                    await bot.send_message(query.from_user.id, f"Выбран пользователь: {data[4]} для ядра {data[2]}", reply_markup=keyboard_markup)
                
                
                elif(data[3]=="info"):
                    cursor.execute("SELECT secret FROM db_users WHERE user = '%s'" % (data[4]))
                    connected_ids = cursor.fetchone()
                    await bot.send_message(query.from_user.id, f"Пользователь: {data[4]} \nПароль: <span class='tg-spoiler'>{connected_ids[0]}</span>", parse_mode="HTML")
                
                
                elif(data[3]=="drop"):
                    cursor.execute("SELECT db_type, db_location FROM db_users WHERE user = '%s';" % (data[4]))
                    connected_ids = cursor.fetchone()
                    cursor.execute("DELETE FROM db_users WHERE user = '%s';" % (data[4]))
                    timed = sql_connection(connected_ids[0], connected_ids[1])
                    timed_curr = timed.cursor
                    if (connected_ids[0]=="postgre"):
                        timed_curr.execute("DROP USER "+str(data[4])+";")
                        timed.cnx_v.commit()
                        timed.cnx_v.close()
                        timed_curr.close()
                    else:
                        timed_curr.execute("REVOKE ALL PRIVILEGES, GRANT OPTION FROM '"+str(data[4])+"'@'%';")
                        timed_curr.execute("DROP USER '"+str(data[4])+"'@'%';")
                        timed.cnx_v.commit()
                        timed.cnx_v.close()
                        timed_curr.close()
                    await bot.send_message(query.from_user.id, f"Пользователь: {data[4]} удален успешно.\nСхемы и таблицы остаются даже если вы удалили всех пользователей.\nЧтобы получить к ним доступ создайте нового пользователя и передайте ему права на использование нужных элементов.", parse_mode="HTML")
                
                
                elif(data[3]=="grant" or data[3]=="g"):
                    if(data[5]=="see"):
                        keyboard_markup = types.InlineKeyboardMarkup(row_width=3, one_time_keyboard=True)
                        keyboard_markup.add(types.InlineKeyboardButton("Schema", callback_data=f"db/s/users/s/{data[2]}/s/grant/s/{data[4]}/s/sch/s/see"))
                        keyboard_markup.add(types.InlineKeyboardButton("IDB", callback_data=f"db/s/users/s/{data[2]}/s/grant/s/{data[4]}/s/idb/s/see"))
                        await bot.send_message(query.from_user.id, f"Выбирите тип: ", parse_mode="HTML", reply_markup=keyboard_markup)
                    elif(data[5]=="sch"):
                        if (data[6]=="see"):
                            cursor.execute("SELECT schema_name, db_type, db_location FROM db_schemas WHERE tg = %i AND db_type = '%s'" % (query.from_user.id,data[2]))
                            connected_ids = cursor.fetchall()
                            cursor.execute("SELECT max_schemas_db FROM users_data WHERE t_id=%i" % (query.from_user.id))
                            slots = cursor.fetchone()
                            keyboard_markup = types.InlineKeyboardMarkup(row_width=3, one_time_keyboard=True)
                            if len(connected_ids)==0:
                                keyboard_markup.add(types.InlineKeyboardButton(f"Создать схему[{slots[0]}]", callback_data="db/s/schemas/s/create/s/create"))
                                await bot.send_message(query.from_user.id, f"У вас нет схем, хотите создать?", reply_markup=keyboard_markup)
                                return
                            for i in range(len(connected_ids)):
                                connected_ids[i] = [connected_ids[i][0],connected_ids[i][1],connected_ids[i][2]]
                            for i in connected_ids:
                                keyboard_markup.add(types.InlineKeyboardButton(str(i[1])+":"+str(i[0]), callback_data=f"db/s/users/s/{data[2]}/s/g/s/{data[4]}/s/sch/s/su/s/{i[0]}"))
                            await bot.send_message(query.from_user.id, f"Выбирите бд: ", parse_mode="HTML", reply_markup=keyboard_markup)
                        elif (data[6]=="su"):
                            keyboard_markup = types.InlineKeyboardMarkup(row_width=3, one_time_keyboard=True)
                            if (data[2]=="postgre"):
                                keyboard_markup.add(types.InlineKeyboardButton("select", callback_data=f"db/s/bb/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/sel"))
                                keyboard_markup.add(types.InlineKeyboardButton("insert", callback_data=f"db/s/bb/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/ins"))
                                keyboard_markup.add(types.InlineKeyboardButton("update", callback_data=f"db/s/bb/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/upp"))
                                keyboard_markup.add(types.InlineKeyboardButton("delete", callback_data=f"db/s/bb/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/del"))
                                keyboard_markup.add(types.InlineKeyboardButton("truncate", callback_data=f"db/s/bb/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/tru"))
                                keyboard_markup.add(types.InlineKeyboardButton("references", callback_data=f"db/s/bb/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/ref"))
                                keyboard_markup.add(types.InlineKeyboardButton("create", callback_data=f"db/s/bb/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/cre"))
                                keyboard_markup.add(types.InlineKeyboardButton("all of this", callback_data=f"db/s/bb/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/all"))
                            else:
                                keyboard_markup.add(types.InlineKeyboardButton("select", callback_data=f"db/s/bb/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/sel"))
                                keyboard_markup.add(types.InlineKeyboardButton("insert", callback_data=f"db/s/bb/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/ins"))
                                keyboard_markup.add(types.InlineKeyboardButton("update", callback_data=f"db/s/bb/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/upp"))
                                keyboard_markup.add(types.InlineKeyboardButton("delete", callback_data=f"db/s/bb/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/del"))
                                keyboard_markup.add(types.InlineKeyboardButton("drop", callback_data=f"db/s/bb/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/dro"))
                                keyboard_markup.add(types.InlineKeyboardButton("references", callback_data=f"db/s/bb/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/ref"))
                                keyboard_markup.add(types.InlineKeyboardButton("create", callback_data=f"db/s/bb/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/cre"))
                                keyboard_markup.add(types.InlineKeyboardButton("alter", callback_data=f"db/s/bb/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/alt"))
                                keyboard_markup.add(types.InlineKeyboardButton("all of this", callback_data=f"db/s/bb/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/all"))
                            await bot.send_message(query.from_user.id, f"Выбирите привилегии: ", parse_mode="HTML", reply_markup=keyboard_markup)
                            
                    elif(data[5]=="idb"):
                        if (data[6]=="see"):
                            cursor.execute("SELECT db, db_type, db_location FROM db_db WHERE tg = %i AND db_type = '%s'" % (query.from_user.id,data[2]))
                            connected_ids = cursor.fetchall()
                            cursor.execute("SELECT max_single_db FROM users_data WHERE t_id=%i" % (query.from_user.id))
                            slots = cursor.fetchone()
                            keyboard_markup = types.InlineKeyboardMarkup(row_width=3, one_time_keyboard=True)
                            if len(connected_ids)==0:
                                keyboard_markup.add(types.InlineKeyboardButton(f"Создать таблицу[{slots[0]}]", callback_data="db/s/schemas/s/create/s/create"))
                                await bot.send_message(query.from_user.id, f"У вас нет таблиц, хотите создать?", reply_markup=keyboard_markup)
                                return
                            for i in range(len(connected_ids)):
                                connected_ids[i] = [connected_ids[i][0],connected_ids[i][1],connected_ids[i][2]]
                            for i in connected_ids:
                                keyboard_markup.add(types.InlineKeyboardButton(str(i[1])+":"+str(i[0])+":"+str(i[2]), callback_data=f"db/s/users/s/{data[2]}/s/g/s/{data[4]}/s/idb/s/su/s/{i[0]}"))
                            await bot.send_message(query.from_user.id, f"Выбирите бд: ", parse_mode="HTML", reply_markup=keyboard_markup)
                        elif (data[6]=="su"):
                            keyboard_markup = types.InlineKeyboardMarkup(row_width=3, one_time_keyboard=True)
                            if (data[2]=="postgre"):
                                keyboard_markup.add(types.InlineKeyboardButton("select", callback_data=f"db/s/bn/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/sel"))
                                keyboard_markup.add(types.InlineKeyboardButton("insert", callback_data=f"db/s/bn/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/ins"))
                                keyboard_markup.add(types.InlineKeyboardButton("update", callback_data=f"db/s/bn/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/upp"))
                                keyboard_markup.add(types.InlineKeyboardButton("truncate", callback_data=f"db/s/bn/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/tru"))
                                keyboard_markup.add(types.InlineKeyboardButton("references", callback_data=f"db/s/bn/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/ref"))
                                keyboard_markup.add(types.InlineKeyboardButton("all of this", callback_data=f"db/s/bn/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/all"))
                            else:
                                keyboard_markup.add(types.InlineKeyboardButton("select", callback_data=f"db/s/bn/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/sel"))
                                keyboard_markup.add(types.InlineKeyboardButton("insert", callback_data=f"db/s/bn/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/ins"))
                                keyboard_markup.add(types.InlineKeyboardButton("update", callback_data=f"db/s/bn/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/upp"))
                                keyboard_markup.add(types.InlineKeyboardButton("delete", callback_data=f"db/s/bn/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/del"))
                                keyboard_markup.add(types.InlineKeyboardButton("references", callback_data=f"db/s/bn/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/ref"))
                                keyboard_markup.add(types.InlineKeyboardButton("alter", callback_data=f"db/s/bn/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/alt"))
                                keyboard_markup.add(types.InlineKeyboardButton("all of this", callback_data=f"db/s/bn/s/{data[2]}/s/{data[4]}/s/{data[7]}/s/all"))
                            await bot.send_message(query.from_user.id, f"Выбирите привилегии: ", parse_mode="HTML", reply_markup=keyboard_markup)
                
                elif(data[3]=="revoke" or data[3]=="r"):
                    ...    
                
                
                elif(data[3]=="passreset"):
                    alphabet = string.ascii_letters + string.digits
                    while True:
                        secret_pass = ''.join(secrets.choice(alphabet) for i in range(24))
                        if (any(c.islower() for c in secret_pass)
                                and any(c.isupper() for c in secret_pass)
                                and sum(c.isdigit() for c in secret_pass) >= 3):
                            break
                    cursor.execute("SELECT db_type, db_location FROM db_users WHERE user = '%s';" % (data[4]))
                    connected_ids = cursor.fetchone()
                    connected_ids = connected_ids[0]
                    cursor.execute("UPDATE db_users SET secret = '%s' WHERE user = '%s';" % (secret_pass,data[4]))
                    timed = sql_connection(connected_ids[0], connected_ids[1])
                    timed_curr = timed.cursor
                    if (connected_ids[0]=="postgre"):
                        timed_curr.execute("ALTER USER '"+str(data[4])+"' WITH PASSWORD '"+str(secret_pass)+"';")
                        timed.cnx_v.commit()
                        timed.cnx_v.close()
                        timed_curr.close()
                    else:
                        timed_curr.execute("ALTER USER '"+str(data[4])+"'@'%' IDENTIFIED BY '"+str(secret_pass)+"';")
                        timed.cnx_v.commit()
                        timed.cnx_v.close()
                        timed_curr.close()

                    await bot.send_message(query.from_user.id, f"Пользователь: {data[4]} \nПароль: <span class='tg-spoiler'>{connected_ids[0]}</span>", parse_mode="HTML")
            
        
        elif(data[1]=="bb"):
            cursor.execute("SELECT db_location FROM db_users WHERE tg = %i AND db_type = '%s' AND user = '%s'" % (query.from_user.id,data[2],data[3]))
            loc = cursor.fetchone()
            timed = sql_connection(data[2], loc[0])
            timed_curr = timed.cursor                           
            if (data[2]=="postgre"):
                if (data[5]=="sel"):
                    timed_curr.execute("GRANT SELECT ON ALL TABLES IN SCHEMA %s TO %s;" % (data[4],data[3]))
                elif(data[5]=="ins"):
                    timed_curr.execute("GRANT INSERT ON ALL TABLES IN SCHEMA %s TO %s;" % (data[4],data[3]))
                elif(data[5]=="upp"):
                    timed_curr.execute("GRANT UPDATE ON ALL TABLES IN SCHEMA %s TO %s;" % (data[4],data[3]))
                elif(data[5]=="del"):
                    timed_curr.execute("GRANT DELETE ON ALL TABLES IN SCHEMA %s TO %s;" % (data[4],data[3]))
                elif(data[5]=="tru"):
                    timed_curr.execute("GRANT TRUNCATE ON ALL TABLES IN SCHEMA %s TO %s;" % (data[4],data[3]))
                elif(data[5]=="ref"):
                    timed_curr.execute("GRANT REFERENCES ON ALL TABLES IN SCHEMA %s TO %s;" % (data[4],data[3]))
                elif(data[5]=="cre"):
                    timed_curr.execute("GRANT CREATE ON SCHEMA %s TO %s;" % (data[4],data[3]))
                elif(data[5]=="all"):
                    timed_curr.execute("GRANT SELECT, INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES ON ALL TABLES IN SCHEMA %s TO %s;" % (data[4],data[3]))
                    timed_curr.execute("GRANT CREATE ON SCHEMA %s TO %s;" % (data[4],data[3]))
            elif(data[2]=="mysql" or data[2]=="mariadb"):
                if (data[5]=="sel"):
                    timed_curr.execute(f"GRANT SELECT ON {data[4]}.* TO '{data[3]}'@'%';")
                elif(data[5]=="ins"):
                    timed_curr.execute(f"GRANT INSERT ON {data[4]}.* TO '{data[3]}'@'%';")
                elif(data[5]=="upp"):
                    timed_curr.execute(f"GRANT UPDATE OON {data[4]}.* TO '{data[3]}'@'%';")
                elif(data[5]=="del"):
                    timed_curr.execute(f"GRANT DELETE ON {data[4]}.* TO '{data[3]}'@'%';")
                elif(data[5]=="dro"):
                    timed_curr.execute(f"GRANT DROP ON {data[4]}.* TO '{data[3]}'@'%';")
                elif(data[5]=="ref"):
                    timed_curr.execute(f"GRANT REFERENCES ON {data[4]}.* TO '{data[3]}'@'%';")
                elif(data[5]=="cre"):
                    timed_curr.execute(f"GRANT CREATE ON {data[4]}.* TO '{data[3]}'@'%';")
                elif(data[5]=="alt"):
                    timed_curr.execute(f"GRANT ALTER ON {data[4]}.* TO '{data[3]}'@'%';")
                elif(data[5]=="all"):
                    timed_curr.execute(f"GRANT SELECT, INSERT, UPDATE, DELETE, DROP, REFERENCES, CREATE, ALTER ON {data[4]}.* TO '{data[3]}'@'%';")
                timed_curr.execute("FLUSH PRIVILEGES;")
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            await bot.send_message(query.from_user.id, f"Права обновлены успешно.", parse_mode="HTML")
        
        elif(data[1]=="bn"):
            cursor.execute("SELECT db_location FROM db_users WHERE tg = %i AND db_type = '%s' AND user = '%s'" % (query.from_user.id,data[2],data[3]))
            loc = cursor.fetchone()
            timed = sql_connection(data[2]+"x", loc[0])
            timed_curr = timed.cursor                           
            if (data[2]=="postgre"):
                if (data[5]=="sel"):
                    timed_curr.execute("GRANT SELECT ON TABLE %s TO %s;" % (data[4],data[3]))
                elif(data[5]=="ins"):
                    timed_curr.execute("GRANT INSERT ON TABLE %s TO %s;" % (data[4],data[3]))
                elif(data[5]=="upp"):
                    timed_curr.execute("GRANT UPDATE ON TABLE %s TO %s;" % (data[4],data[3]))
                elif(data[5]=="tru"):
                    timed_curr.execute("GRANT TRUNCATE ON TABLE %s TO %s;" % (data[4],data[3]))
                elif(data[5]=="ref"):
                    timed_curr.execute("GRANT REFERENCES ON TABLE %s TO %s;" % (data[4],data[3]))
                elif(data[5]=="all"):
                    timed_curr.execute("GRANT SELECT, INSERT, UPDATE, TRUNCATE, REFERENCES ON TABLE isolate.%s TO %s;" % (data[4],data[3]))
            elif(data[2]=="mysql" or data[2]=="mariadb"):
                if (data[5]=="sel"):
                    timed_curr.execute(f"GRANT SELECT ON {data[4]}.* TO '{data[3]}'@'%';")
                elif(data[5]=="ins"):
                    timed_curr.execute(f"GRANT INSERT ON {data[4]}.* TO '{data[3]}'@'%';")
                elif(data[5]=="upp"):
                    timed_curr.execute(f"GRANT UPDATE OON {data[4]}.* TO '{data[3]}'@'%';")
                elif(data[5]=="del"):
                    timed_curr.execute(f"GRANT DELETE ON {data[4]}.* TO '{data[3]}'@'%';")
                elif(data[5]=="ref"):
                    timed_curr.execute(f"GRANT REFERENCES ON {data[4]}.* TO '{data[3]}'@'%';")
                elif(data[5]=="alt"):
                    timed_curr.execute(f"GRANT ALTER ON {data[4]}.* TO '{data[3]}'@'%';")
                elif(data[5]=="all"):
                    timed_curr.execute(f"GRANT SELECT, INSERT, UPDATE, DELETE, REFERENCES, ALTER ON isolate.{data[4]} TO '{data[3]}'@'%';")
                timed_curr.execute("FLUSH PRIVILEGES;")
            timed.cnx_v.commit()
            timed.cnx_v.close()
            timed_curr.close()
            await bot.send_message(query.from_user.id, f"Права обновлены успешно.", parse_mode="HTML")
        
        elif(data[1]=="schemas"):
            if (data[2]=="schemas"):
                cursor.execute("SELECT schema_name, db_type FROM db_schemas WHERE tg = %i" % (query.from_user.id))
                connected_ids = cursor.fetchall()
                cursor.execute("SELECT max_schemas_db FROM users_data WHERE t_id = %i" % (query.from_user.id))
                slots = cursor.fetchone()
                keyboard_markup = types.InlineKeyboardMarkup(row_width=3, one_time_keyboard=True)
                if len(connected_ids)==0:
                    keyboard_markup.add(types.InlineKeyboardButton(f"Создать схему[{slots[0]}]", callback_data="db/s/schemas/s/create/s/create"))
                    await bot.send_message(query.from_user.id, f"У вас нет схем, хотите создать?", reply_markup=keyboard_markup)
                    return
                for i in range(len(connected_ids)):
                    connected_ids[i] = [connected_ids[i][0],connected_ids[i][1]]
                for i in connected_ids:
                    keyboard_markup.add(types.InlineKeyboardButton(str(i[1])+":"+str(i[0]), callback_data=f"db/s/schemas/s/{i[1]}/s/see/s/{i[0]}"))
                if (slots[0]-len(connected_ids)>0):
                    keyboard_markup.add(types.InlineKeyboardButton(f"Создать схему[{slots[0]-len(connected_ids)}]", callback_data="db/s/schemas/s/create/s/create"))
                await bot.send_message(query.from_user.id, f"Ваши схемы:", reply_markup=keyboard_markup)    
                    
            
            
            
            
            elif(data[2]=="create"):
                markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
                markup.add("MySQL")
                markup.add("PostGre")
                markup.add("MariaDB")
                markup.add("Отмена")
                await create_schema_sql.db_core.set()
                await bot.send_message(query.from_user.id, f"Доступные движки:", reply_markup=markup) 
            
            
            
            
            
            elif(data[2]=="mysql" or data[2]=="postgre" or data[2]=="mariadb"):
                if(data[3]=="see"):
                    keyboard_markup = types.InlineKeyboardMarkup(row_width=3, one_time_keyboard=True)
                    keyboard_markup.add(types.InlineKeyboardButton("Info", callback_data=f"db/s/schemas/s/{data[2]}/s/info/s/{data[4]}"))
                    keyboard_markup.add(types.InlineKeyboardButton("Reset schema", callback_data=f"db/s/schemas/s/{data[2]}/s/reset/s/{data[4]}"))
                    keyboard_markup.add(types.InlineKeyboardButton("Drop", callback_data=f"db/s/schemas/s/{data[2]}/s/drop/s/{data[4]}"))
                    await bot.send_message(query.from_user.id, f"Выбрана схема: {data[4]} для ядра {data[2]}", reply_markup=keyboard_markup)
                
                
                elif(data[3]=="info"):
                    #Dropped
                    await bot.send_message(query.from_user.id, f"Тестовый функционал. [Не доступно]", parse_mode="HTML")
                
                
                elif(data[3]=="drop"):
                    cursor.execute("SELECT db_location FROM db_schemas WHERE tg = %i AND db_type = '%s' AND schema_name = '%s'" % (query.from_user.id,data[2],data[4]))
                    loc = cursor.fetchone()
                    timed = sql_connection(data[2], loc[0])
                    timed_curr = timed.cursor  
                    if(data[2]=="postgre"):
                        cursor.execute("DELETE FROM db_schemas WHERE schema_name = '%s';" % (data[4]))
                        timed_curr.execute("DROP SCHEMA "+str(data[4])+" CASCADE;")
                    elif(data[2]=="mysql" or data[2]=="mariadb"):
                        cursor.execute("DELETE FROM db_schemas WHERE schema_name = '%s';" % (data[4]))
                        timed_curr.execute("DROP SCHEMA "+str(data[4])+" CASCADE;")
                    timed.cnx_v.commit()
                    timed.cnx_v.close()
                    timed_curr.close()
                    await bot.send_message(query.from_user.id, f"Схема: {data[4]} удалена успешно.", parse_mode="HTML")
                
                
                elif(data[3]=="reset"):
                    #Dropped
                    await bot.send_message(query.from_user.id, f"Тестовый функционал. [Не доступно]", parse_mode="HTML")
        
        
        
        
        
        elif(data[1]=="databases" or data[1]=="tab"):
            if (data[2]=="databases"):
                cursor.execute("SELECT db, db_type FROM db_db WHERE tg = %i" % (query.from_user.id))
                connected_ids = cursor.fetchall()
                cursor.execute("SELECT max_single_db FROM users_data WHERE t_id = %i" % (query.from_user.id))
                slots = cursor.fetchone()
                keyboard_markup = types.InlineKeyboardMarkup(row_width=3, one_time_keyboard=True)
                if len(connected_ids)==0:
                    keyboard_markup.add(types.InlineKeyboardButton(f"Создать изолированную БД[{slots[0]}]", callback_data="db/s/tab/s/create"))
                    await bot.send_message(query.from_user.id, f"У вас нет изолированных БД, хотите создать?", reply_markup=keyboard_markup)
                    return
                for i in range(len(connected_ids)):
                    connected_ids[i] = [connected_ids[i][0],connected_ids[i][1]]
                for i in connected_ids:
                    keyboard_markup.add(types.InlineKeyboardButton(str(i[1])+":"+str(i[0]), callback_data=f"db/s/tab/s/{i[1]}/s/see/s/{i[0]}"))
                if (slots[0]-len(connected_ids)>0):
                    keyboard_markup.add(types.InlineKeyboardButton(f"Создать изолированную БД[{slots[0]-len(connected_ids)}]", callback_data="db/s/tab/s/create"))
                await bot.send_message(query.from_user.id, f"Ваши ИБД:", reply_markup=keyboard_markup)    
                    
            
            
            
            
            elif(data[2]=="create"):
                markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
                markup.add("MySQL")
                markup.add("PostGre")
                markup.add("MariaDB")
                markup.add("Отмена")
                await create_table_sql.db_core.set()
                await bot.send_message(query.from_user.id, f"Доступные движки:", reply_markup=markup) 
            
            
            
            
            elif(data[2]=="mysql" or data[2]=="postgre" or data[2]=="mariadb"):
                if(data[3]=="see"):
                    keyboard_markup = types.InlineKeyboardMarkup(row_width=3, one_time_keyboard=True)
                    keyboard_markup.add(types.InlineKeyboardButton("Info", callback_data=f"db/s/databases/s/{data[2]}/s/info/s/{data[4]}"))
                    keyboard_markup.add(types.InlineKeyboardButton("Reset DB", callback_data=f"db/s/databases/s/{data[2]}/s/reset/s/{data[4]}"))
                    keyboard_markup.add(types.InlineKeyboardButton("Drop", callback_data=f"db/s/databases/s/{data[2]}/s/drop/s/{data[4]}"))
                    await bot.send_message(query.from_user.id, f"Выбрана ИБД: {data[4]} для ядра {data[2]}", reply_markup=keyboard_markup)
                
                elif(data[3]=="info"):
                    #Dropped
                    await bot.send_message(query.from_user.id, f"Тестовый функционал. [Не доступно]", parse_mode="HTML")
                
                elif(data[3]=="drop"):
                    cursor.execute("SELECT db_location FROM db_db WHERE tg = %i AND db_type = '%s' AND db = '%s'" % (query.from_user.id,data[2],data[4]))
                    loc = cursor.fetchone()
                    timed = sql_connection(data[2]+"x", loc[0])
                    timed_curr = timed.cursor  
                    if(data[2]=="postgre"):
                        cursor.execute("DELETE FROM db_db WHERE db = '%s';" % (data[4]))
                        timed_curr.execute("DROP TABLE isolate."+str(data[4])+";")
                    elif(data[2]=="mysql" or data[2]=="mariadb"):
                        cursor.execute("DELETE FROM db_db WHERE db = '%s';" % (data[4]))
                        timed_curr.execute("DROP TABLE isolate."+str(data[4])+";")
                    timed.cnx_v.commit()
                    timed.cnx_v.close()
                    timed_curr.close()
                    await bot.send_message(query.from_user.id, f"Таблица: {data[4]} удалена успешно.", parse_mode="HTML")
                
                elif(data[3]=="reset"):
                    #Dropped
                    await bot.send_message(query.from_user.id, f"Тестовый функционал. [Не доступно]", parse_mode="HTML")

        
        else:
            await bot.send_message(query.from_user.id, "Ошибка, возможно мы ее уже чиним...")
    
    
    
    
    
    
    
    else:
        await bot.send_message(query.from_user.id, "Попробуйте использовать команду еще раз.")



######################
######################
######################

@dp.message_handler()
async def send_message(msg: types.Message):
    if (msg.text=='Avrest Data'):
        keyboard_markup = types.InlineKeyboardMarkup(row_width=3, one_time_keyboard=True)
        keyboard_markup.add(types.InlineKeyboardButton("Users", callback_data="db/s/users/s/users"))
        keyboard_markup.add(types.InlineKeyboardButton("Schemas", callback_data="db/s/schemas/s/schemas"))
        keyboard_markup.add(types.InlineKeyboardButton("DataBases", callback_data="db/s/databases/s/databases"))
        await msg.answer("Текущие ресурсы:", reply_markup=keyboard_markup)
    elif (msg.text=='Avrest on Discord'):
        await msg.answer("Данный функционал в данный момент недоступен, чтобы получать доступ к тестовым функциям и дополнительным плюшкам от AGuild запишитесь в тестовую программу! [/gotest]")
    elif (msg.text=='Avrest Click'):
        await msg.answer("Данный функционал в данный момент недоступен, чтобы получать доступ к тестовым функциям и дополнительным плюшкам от AGuild запишитесь в тестовую программу! [/gotest]")
    else:
        ...



if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
